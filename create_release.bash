#!/bin/bash

version="${1}"
install_files=(studiosh studiosh.py gui LICENSE doc)
package_dir="/tmp/studiosh_package"
package_file="${package_dir}_${version}.tar.xz"

if [ -z "${version}" ]; then
	echo "Error: missing mandatory argument 'version' (e. g.: 0.1, 1.2, ...). Exiting."
	exit 1
fi

echo "Creating ${package_dir}... "
if [ -d "${package_dir}" ]; then
	rm -r "${package_dir}"
fi 
mkdir "${package_dir}"
echo ""

echo "Installing config & sessions directories... "
./install.bash force >/dev/null
echo ""

echo "Copying files to package dir..."
for f in "${install_files[@]}"; do 
	cp -r "${f}" "${package_dir}"
done
echo ""

echo "Creating package: ${package_file}... "
tar cf ${package_file} -C "$(dirname ${package_dir})" "$(basename ${package_dir})"
rm -r ${package_dir}
echo ""

if [ -f "${package_file}" ]; then
	username="baukleberbigband"
	package_url="https://gitlab.com/api/v4/projects/45713536/packages/generic/studiosh_package/${version}/${package_file##\/tmp/}"
	echo "package_url: ${package_url}"
	echo ""
	
	read -sp "Token for ${username}: " token
	echo ""
	
	echo "Uploading package to gitlab repository... "
	echo ""
	if ! curl -u "${username}:${token}" --upload-file ${package_file} "${package_url}"; then
		error_code=$?
		echo ""
		echo "Error: upload failed: ${error_code}."
		echo ""
		exit "${error_code}"
	else
		echo ""
		echo ""
		echo "Creating new release... "
		echo ""
		
		data_name="studiosh ${version}"
		data_tag_name="${version}"
		data_tag_message="studiosh ${version} release"
		data_ref="main"
		data_description="studiosh version ${version} release"
		data_assets_links_name="studiosh_package_${version}"
		data_assets_links_url="${package_url}"
		data_assets_links_direct_asset_path="/studiosh_package_${version}"
		data="{ \"name\": \"${data_name}\", \"tag_name\": \"${data_tag_name}\", \"tag_message\": \"${data_tag_message}\", \"ref\": \"${data_ref}\", \"description\": \"${data_description}\", \"assets\": { \"links\": [{ \"name\": \"${data_assets_links_name}\", \"url\": \"${data_assets_links_url}\", \"direct_asset_path\": \"${data_assets_links_direct_asset_path}\", \"link_type\": \"package\" }] } }"
		echo "data: ${data}"
		echo ""
		
		if ! curl --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: ${token}" --data "${data}" --request POST "https://gitlab.com/api/v4/projects/45713536/releases"; then
			error_code=$?
			echo ""
			echo "Error: creating release failed: ${error_code}."
			echo ""
			exit "${error_code}"
		fi
	fi
	echo ""
else
	echo "Error: package ${package_file} not found."
	echo "Exiting."
	exit 1
fi

rm ${package_file}

echo ""
echo "Finished."
