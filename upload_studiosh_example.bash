#!/bin/bash

example_file="studiosh_example.tar.xz"

tar cf /tmp/${example_file} ${example_file%%.tar.xz}
curl -u baukleberbigband --upload-file /tmp/${example_file} "https://gitlab.com/api/v4/projects/45713536/packages/generic/studiosh_example/1.0/${example_file}"
rm /tmp/${example_file}

