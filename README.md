# studiosh

## Description
Studiosh is a basic session manager for GUN/Linux (not tested on other platforms), that can start different audio programs and interconnect them to your needs. It started as a basic shell script for re/starting looping sessions (using [Sooperlooper](https://sonosaurus.com/sooperlooper/index.html), with a custom GUI made with [Open Stage Control](https://openstagecontrol.ammd.net/)) in the form of a shell (command line interface) - hence studiosh - and was later rewritten in Python. In it's current form, it's more or less designed to suite the workflow of bands the author of studiosh is involved in, but maybe parts of it or even whole project can serve others.

## Installation
First install studiosh dependecies - on Ubuntu, you can use following command:
* `sudo pip install pyyaml `
* if installing using pip fails, you can try to install the dependencies using Ubuntu packages, for example: `sudo apt install python3-rtmidi`

To run studiosh with the example configuration, you also need to have following programs installed:
* [Sooperlooper](https://sonosaurus.com/sooperlooper/download.html)
* [Open Stage Control](https://openstagecontrol.ammd.net/download/)
* [Qjackctl](https://qjackctl.sourceforge.io/qjackctl-downloads.html#Downloads)
* [Ardour](https://community.ardour.org/download) (neccessary only for `recorder` command)
* on Ubuntu, run these commands to install them: 
    * `sudo apt install sooperlooper qjackctl ardour`
    * download [open-stage-control-1.15.4-amd64.deb](https://github.com/jean-emmanuel/open-stage-control/releases/download/v1.15.4/open-stage-control-1.15.4-amd64.deb) and run `sudo dpkg -i ~/Downloads/open-stage-control-1.15.4-amd64.deb`

Then, download [sutdiosh package](https://gitlab.com/baukleberbigband/studiosh/-/packages) and unpack it somewhere, e. g.: `tar xf ~/Downloads/studiosh_package_X.X.tar.xz`.

## Basic usage
When in unpacked studiosh package directory (use `cd ~/Downloads/studiosh_package` to move there), run: `./studiosh` - it should start a shell interface (see image below), where you then can run studiosh commands, as documented [here](./doc/commands.txt). For starter, you can try these:
* `sessions` - to show available sessions
* `start` or `start <session name>` - to start a session

![Image](./doc/studiosh.png)

Starting a session should start programs defined in studiosh [configuration file](./studiosh_example/config/studiosh.yaml). This configuration file consists of several sections which define what studiosh does when starting, changing session or song, or starting recorder program (as [explained here](...)). Unless you run system similar to the one studiosh is tested on - Ubuntu - it will be necessary to update all `connectNodes` directives in this configuration file. They tell studiosh what connections to make between programs and between them and system's sound inputs and outputs. Directive `connectNodes` consists of a list (each element separated by a hyphen (-) mark) of connections to make in the form of: `'source:port': 'destination:port'`. So in case, you run different system than Ubuntu, you will probably need to update all `'system:capture_1'`, `'system:capture_2'`, `'system:playback_1'` and `'system:playback_2'` parts according to the names of your sound inputs and outputs (to figure them out, use programs like qjackctl - see image below, qpwgraph, etc.).

![Image](./doc/studiosh_connections.png)

One of the programs defined in studiosh package configuration file is custom GUI for Sooperlooper looping program - see image below. You can use it for starting/stopping loops, changing songs, changing sessions, recording etc.

![Image](./doc/studiosh_gui.png)

You can change several attributes of the tracks in studiosh GUI - their names, colour and whether they are synced to sync track (see below). All of that is defined in studiosh GUI [configuration file](./studiosh_example/config/studiosh_gui.yaml) in section `tracks_attributes` which consists of a list of sessions (band or project), each of which consists of a list of songs, each of which consists of a list of tracks. Currently, first track serves as a synchronization track - when played, it triggers all other tracks to play (in loop or once mode, see below) after a time defined in `tracks_autoplay_sync` directive in studiosh GUI configuration file. If 3rd attribute of a track is `false`, it won't start, if it's `once`, it will loop just once, if it's `loop`, it will loop indefinetly. Second track is meant to be a click track musicians (usually drummer). Third track is meant to be a background loop to play to and the rest of the tracks are tracks for specific instruments to loop to. But it's up to the user to use these features or not.

The possibility to have a custom GUI for Sooperlooper is thanks to Sooperlooper author's great idea to split it to two parts - engine and GUI. They then communicate using [OSC (Open Sound Control) protocol](https://en.wikipedia.org/wiki/Open_Sound_Control) and that's what studiosh GUI uses too. The OSC commands for Sooperlooper are documented [here](https://sonosaurus.com/sooperlooper/doc_osc.html). Currently, there is no easy way to just replace studiosh GUI with original Sooperlooper GUI, cause studiosh GUI plays a role in reloading Sooperlooper's sessions (that may change in the future), but you can run Sooperlooper GUI alongside studiosh one just by running `slgui` - when everything is up and running, it should connect to Sooperlooper engine.

As opposed to Sooperlooper GUI, studiosh GUI doesn't currently allow to manipulate volume knobs and tracks playhead. Instead, Sooperlooper allows you to define MIDI bindings for MIDI controller in a file with extension of .slb and for studiosh to properly load it upon session start/change, it must be present at the path `sessions/<session_name>/<session_name>.slb`. Usually you create and edit this file using Sooperlooper's MIDI [bindings editor](https://sonosaurus.com/sooperlooper/doc_midi.html). The MIDI bindings defined in studiosh package are set up to work with KORG nanoPad, KORG nanoKontrol and BOSS DD500 (digital delay with ability to send MIDI messages).

In `sessions/<session_name>/` there are also files with extensions .slsess, which are Sooperlooper's session files defining what tracks any session consists of and what audio loops to load into them (if any).

Happy looping!

## Support
In case of any issues and/or feature requests, please create an [issue](https://gitlab.com/baukleberbigband/studiosh/-/issues).

## License
Studiosh is released under GNU General Public License version 3 - see [LICENSE](./LICENSE).
