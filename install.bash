#!/bin/bash

# variables
force=false
answer="yes"
example_file="studiosh_example.tar.xz"
user_root_dir="/tmp/studiosh_package"

# functions
function readUserAnswer() {
	msg="$1"
	options=($2)
	options_print="$2"
	default_value=${options[0]}
	
	while true; do
		read -rei "${default_value}" -p "${msg} Type ${options_print// / or }: " answer

		for o in ${options[@]}; do
			if [ "${o}" == "${answer}" ]; then
				echo "$answer"
				return 0
			fi
		done

		echo "Unrecognized answear: '${answer}'. Type ${options_print// / or }." >&2
	done
}

function readUserValue() {
	msg="$1"
	default_val="$2"

	while true; do
		read -rei "${default_val}"  -p "${msg}" answer

		if [ -z "${answer}" ]; then
			echo "Your response can't be empty. Try again." >&2
		else
			echo "${answer}"
			break
		fi
	done
}

function downloadExample() {	
	downloaded=0
	
	if [ ! -f "/tmp/${example_file}" ]; then
		echo -n "Downloading ${example_file}... "
		cd /tmp 2>/dev/null && \
		wget https://gitlab.com/baukleberbigband/studiosh/-/package_files/77971268/download -O ${example_file} 2>/dev/null && \
		tar xf /tmp/${example_file} 2>/dev/null && \
		downloaded=1
		
		if [ ${downloaded} -eq 1 ]; then
			echo "Download sucessful."
		else
			echo "Something went wrong."
		fi
	fi
}

function copyExample() {
	pwd
	rm ./studiosh_example/config/*~ 2>/dev/null
	rm ./studiosh_example/sessions/*~ 2>/dev/null
	cp -r ./studiosh_example /tmp/
}

function updateExampleFiles() {
	# CONFIG FILES
	# studiosh.yaml
	sed -i 's|\./recording|'${user_root_dir}'/recording|g' "${user_root_dir}/config/studiosh.yaml"
	sed -i 's|\./sessions|'${user_root_dir}'/sessions|g' "${user_root_dir}/config/studiosh.yaml"
	
	# studiosh_gui.yaml
	sed -i 's|\./recording|'${user_root_dir}'/recording|g' "${user_root_dir}/config/studiosh_gui.yaml"
	sed -i 's|\./sessions|'${user_root_dir}'/sessions|g' "${user_root_dir}/config/studiosh_gui.yaml"

	# SESSION FILES
	# band1
	sed -i 's|\./sessions/click_16_090bpm.ogg|'${user_root_dir}'/sessions/click_16_090bpm.ogg|g' "${user_root_dir}/sessions/band1/band1_01_song01.slsess"
	sed -i 's|\./sessions/band1/band1_01_song01_click.ogg|'${user_root_dir}'/sessions/band1/band1_01_song01_click.ogg|g' "${user_root_dir}/sessions/band1/band1_01_song01.slsess"
	sed -i 's|\./sessions/band1/band1_01_song01_sounds.ogg|'${user_root_dir}'/sessions/band1/band1_01_song01_sounds.ogg|g' "${user_root_dir}/sessions/band1/band1_01_song01.slsess"

	sed -i 's|\./sessions/click_16_090bpm.ogg|'${user_root_dir}'/sessions/click_16_090bpm.ogg|g' "${user_root_dir}/sessions/band1/band1_02_song02.slsess"
	sed -i 's|\./sessions/band1/band1_02_song02_click.ogg|'${user_root_dir}'/sessions/band1/band1_02_song02_click.ogg|g' "${user_root_dir}/sessions/band1/band1_02_song02.slsess"
	sed -i 's|\./sessions/band1/band1_02_song02_sounds.ogg|'${user_root_dir}'/sessions/band1/band1_02_song02_sounds.ogg|g' "${user_root_dir}/sessions/band1/band1_02_song02.slsess"

	sed -i 's|\./sessions/click_16_090bpm.ogg|'${user_root_dir}'/sessions/click_16_090bpm.ogg|g' "${user_root_dir}/sessions/band1/band1_03_song03.slsess"
	sed -i 's|\./sessions/band1/band1_03_song03_click.ogg|'${user_root_dir}'/sessions/band1/band1_03_song03_click.ogg|g' "${user_root_dir}/sessions/band1/band1_03_song03.slsess"
	sed -i 's|\./sessions/band1/band1_03_song03_sounds.ogg|'${user_root_dir}'/sessions/band1/band1_03_song03_sounds.ogg|g' "${user_root_dir}/sessions/band1/band1_03_song03.slsess"

	# band2
	sed -i 's|\./sessions/click_16_090bpm.ogg|'${user_root_dir}'/sessions/click_16_090bpm.ogg|g' "${user_root_dir}/sessions/band2/band2_01_song01.slsess"
	sed -i 's|\./sessions/band2/band2_01_song01_click.ogg|'${user_root_dir}'/sessions/band2/band2_01_song01_click.ogg|g' "${user_root_dir}/sessions/band2/band2_01_song01.slsess"
	sed -i 's|\./sessions/band2/band2_01_song01_sounds.ogg|'${user_root_dir}'/sessions/band2/band2_01_song01_sounds.ogg|g' "${user_root_dir}/sessions/band2/band2_01_song01.slsess"
}


# main
echo -n "Installing node js yaml module... "
cd ./gui/ && npm install yaml@2.0.0-11
cd ..
echo "done"

if [ "${1}" == "force" ]; then
	force=true
fi

"${force}" || answer=$(readUserAnswer "Would you like to setup example configuration and Sooperlooper sessions (internet connection is necessary)?" "yes no")
if [ "${answer}" == "yes" ]; then
	necessary_programs=(sooperlooper qjackctl open-stage-control ardour7)
	missing_programs=()
	for p in ${necessary_programs[@]}; do
		if ! which "$p" >/dev/null 2>&1; then
			missing_programs+=($p)
		fi
	done

	if [ ${#missing_programs[*]} -gt 0 ]; then
		"${force}" || answer=$(readUserAnswer "Example configuration needs some programs to be installed to run properly, some of which seem to be missing on your system: '${missing_programs}' (you can install them manually, or change the configuration later). Do you wan't to continue?" "yes no")
		if [ "${answer}" == "no" ]; then
			echo "Exiting."
			exit 0
		fi
	fi

	echo "Setting up example configuration and Sooperlooper sessions."

	#downloadExample
	copyExample

	while true; do
		"${force}" || user_root_dir=$(readUserValue "Where do you want to install them (default: ./config/)?: " ".")
		if [ -d "${user_root_dir}" ]; then
			break
		else
			echo "Directory ${user_root_dir} does not exist. Try again."
		fi
	done
	
	user_root_dir="${user_root_dir%%\/}"
	
	cd "${user_root_dir}"

	cp -r /tmp/${example_file%%.tar.xz}/config .
	cp -r /tmp/${example_file%%.tar.xz}/sessions .
else
	echo "Exiting."
	exit 0
fi

#updateExampleFiles

if [ ! -z "${user_root_dir}" ]; then
	echo "Example configuration and Sooperlooper sessions are installed to ${user_root_dir}. Don't forget to run studiosh with ${user_root_dir} as argument: ./studiosh ${user_root_dir}."
fi

if [ -f "/tmp/${example_file}" ]; then
	rm "/tmp/${example_file}"
fi

if [ -d "/tmp/${example_file%%.tar.xz}" ]; then
	rm -r "/tmp/${example_file%%.tar.xz}"
fi

