const {spawn} = nativeRequire("child_process")
const yaml = nativeRequire("yaml")
const fs = nativeRequire("fs")

const CONFIG_FILE = "./config/studiosh_gui.yaml"
const TRACK_HEIGHT = 110
const MIN_DELAY = 1
const osc_host = "localhost:8123"
const sl_host = "localhost"
const sl_port = "9951"
const sl_port_aux = "9952"
const sh_host = sl_host     // studiosh.py
const sh_port = "8100"      // studiosh.py

let debug = true
let config_file = null
let configuration = null
let session_autoplay_delay = null
let sessions_dir = null
let recorder_dir = null
let recorder_dir_tarcks_save = null
let session_name = null
let session_label = null
let song_id = null
let song_name = null
let song_label = null
let tracks_attrs = null
let tracks_cnt = null
let tracks_lens = []
let tracks_states = []
let selected_track = null

function logDebug(msg) {
    if (debug) {
        console.log("Debug: " + msg)
    }
}

function logError(msg) {
    console.log("Error: " + msg)
}

function logInfo(msg) {
    console.log("Info: " + msg)
}

function getCurrentTracksParameters() {
    logDebug("getCurrentTracksParameters")
    let tracks_range = "[0-" + (tracks_cnt - 1) + "]"
    
    // set up general updates
    send(sl_host, sl_port, '/register_update', 'selected_loop_num', osc_host, '/loop_selected')
        
    // set up track updates
    send(sl_host, sl_port, '/sl/' + tracks_range + '/register_update', 'wet', osc_host, '/loop_vol')
    send(sl_host, sl_port, '/sl/' + tracks_range + '/register_auto_update', 'state', 100, osc_host, '/loop_state')
    send(sl_host, sl_port, '/sl/' + tracks_range + '/register_auto_update', 'loop_len', 100, osc_host, '/loop_len')
    send(sl_host, sl_port, '/sl/' + tracks_range + '/register_auto_update', 'loop_pos', 100, osc_host, '/loop_pos')
    
    // get current tracks parameters    
    send(sl_host, sl_port, '/get', 'selected_loop_num', osc_host, '/loop_selected')
    send(sl_host, sl_port, '/sl/' + tracks_range + '/get', 'wet', osc_host, '/loop_vol')
    send(sl_host, sl_port, '/sl/' + tracks_range + '/get', 'state', osc_host, '/loop_state')        
    send(sl_host, sl_port, '/sl/' + tracks_range + '/get', 'loop_len', osc_host, '/loop_len')            
    send(sl_host, sl_port, '/sl/' + tracks_range + '/get', 'loop_pos', osc_host, '/loop_pos')

    receive("/EDIT", "loading", {"visible": false})
    receive("/EDIT", "tracks", {"visible": true})
}

function setUpTracks(tracks_cnt, song_name, song_path) {
    logDebug("tracks_cnt: " + tracks_cnt)
    logDebug("song_name: " + song_name)
    logDebug("song_id: " + song_id)

    if (tracks_cnt > 0 && song_name != "") {
        // set up tracks
        let curr_tracks_attrs = tracks_attrs[session_name][song_name]["tracks"]

        if (curr_tracks_attrs) {
            Object.keys(curr_tracks_attrs).forEach((track_name, idx) => {
                logDebug("track_name: " + track_name)
                logDebug("idx: " + idx)
                let track_color = curr_tracks_attrs[track_name]["color"]
                logDebug("track_color: " + track_color)
                let track_autoplay = curr_tracks_attrs[track_name]["autoplay"]
                logDebug("track_autoplay: " + track_autoplay)
                let track_autoplay_delay = curr_tracks_attrs[track_name]["autoplay_delay"] || tracks_attrs[session_name][song_name]["autoplay_delay"] || session_autoplay_delay || MIN_DELAY
                logDebug("track_autoplay_delay: " + track_autoplay_delay)

                // find file name of loaded loop, if any
                let songs_dir = sessions_dir + "/" + song_name.split("_")[0]
                logDebug("songs_dir: " + songs_dir)
                let song_path = songs_dir + "/" + song_name + ".slsess"
                logDebug("song_path: " + song_path)
                let command = 'grep -h "index=\\"' + idx + '\\"" ' + song_path + '|grep -o \"[^/]*\.\\(wav\\|ogg\\)\" || echo ""'
                logDebug("command: " + command)
                const get_tracks_files = spawn(command, {shell: true})
                get_tracks_files.stdout.on('data', (data) => {
                    let file = "^file-audio no file"
                    let data_string = data.toString()
                    logDebug("data_string: \"" + data_string + "\"")
                    if (data && data_string != "" && data_string != "\n") {
                        file =  "^file-audio " + data.toString()
                    }
                    logDebug("file: " + file)

                    // track attributes
                    // basic info
                    receive("/EDIT/MERGE", "track" + idx, {"variables": {"name": track_name, "file": file, "color": track_color}})
                    
                    // autoplay 
                    receive("/loop"+ idx +"_delay", "")   // clear first
                    if (track_autoplay && track_autoplay_delay >= MIN_DELAY) {
                        receive("/loop"+ idx +"_delay", "^play " + track_autoplay + " ^arrow-right-long " + track_autoplay_delay + "^rotate")
                    }
                })
            })
        }

        // load sooperlooper song into running sooperlooper_main
        // must be here after setUpTracks, to run after gui is prepared
        send(sl_host, sl_port, "/load_session", song_path, "localhost:8123", "/load_session")
        send(sl_host, sl_port, "/load_midi_bindings", sessions_dir + "/" + session_name + "/" + session_name + ".slb", "/load_midi_bindings")
        logDebug("waiting")                

        setTimeout(() => {
            getCurrentTracksParameters()
            }, 10000)
        }
}

function triggerClickAndSounds(track_id, autoplay) {
    logDebug("triggerClickAndSounds")    

    if (autoplay == "once") {
        console.log("triggering track (once): " + track_id)
        send(sl_host, sl_port, "/sl/" + track_id + "/hit", "oneshot")
    }
    else if (autoplay == "loop") {
        console.log("triggering track (trigger): " + track_id)
        send(sl_host, sl_port, "/sl/" + track_id + "/hit", "trigger")
    }
}

module.exports = {
    init: function() {
        config_file = fs.readFileSync(CONFIG_FILE, "utf8")
        configuration = yaml.parse(config_file)
        logDebug("configuration: " + configuration)
        sessions_dir = configuration["general"]["sessions_dir"]
        logDebug("sessions_dir: " + sessions_dir)
        recorder_dir = configuration["general"]["recorder_dir"]
        logDebug("recorder_dir: " + recorder_dir)        
        recorder_dir_tracks_save = configuration["general"]["recorder_dir_tracks_save"]
        logDebug("recorder_dir_tracks_save: " + recorder_dir_tracks_save)        
        session_autoplay_delay = configuration["general"]["autoplay_delay"]
        logDebug("session_autoplay_delay: " + session_autoplay_delay)
        tracks_attrs = configuration["tracks_attributes"]
        logDebug("tracks_attrs: " + tracks_attrs)   
    },

    oscInFilter: function(data) {
        let {address, args, host, port} = data

        if (address == "/loop_state") {
            let track_id = args[0].value
            let track_name = Object.keys(tracks_attrs[session_name][song_name]["tracks"])[track_id]
            let track_state = ((args.length > 2) ? args[2].value : null)

            logDebug("STATE: loop: " + track_id + ", ctrl: " + args[1].value + ", value: " + track_state + ", host: " + host + ", port: " + port)

            switch (track_state) {
                case 0:		// off
                    receive("/EDIT", "loop" + track_id + "_state", {"alphaFillOff": 1})
                    receive("/EDIT", "loop" + track_id + "_state", {"colorText": "#3C2013"})
                    receive("/EDIT", "loop" + track_id + "_state", {"alphaStroke": 0})
                    tracks_states[track_id] = "off"
                    receive("/loop"+ track_id +"_pos", 0)
                    receive("/loop"+ track_id +"_record", "0")
                    receive("/loop"+ track_id +"_pause", "0")
                    receive("/loop"+ track_id +"_mute", "0")
                    receive("/loop"+ track_id +"_trigger", "0")
                    receive("/loop"+ track_id +"_oneshot", "0")
                    receive("/loop"+ track_id +"_solo", "0")
                    break;
                case 1:		// wait start
                    receive("/EDIT", "loop" + track_id + "_state", {"alphaFillOff": 0})
                    receive("/EDIT", "loop" + track_id + "_state", {"colorText": tracks_attrs[session_name][song_name]["tracks"][track_name]["color"]})
                    receive("/EDIT", "loop" + track_id + "_state", {"colorStroke": tracks_attrs[session_name][song_name]["tracks"][track_name]["color"]})
                    receive("/EDIT", "loop" + track_id + "_state", {"alphaStroke": 1})
                    tracks_states[track_id] = "wait start"
                    break;
                case 2:		// recording
                    receive("/EDIT", "loop" + track_id + "_state", {"alphaFillOff": 1})
                    receive("/EDIT", "loop" + track_id + "_state", {"colorText": "#3C2013"})
                    receive("/EDIT", "loop" + track_id + "_state", {"alphaStroke": 0})
                    receive("/loop"+ track_id +"_record", "record")
                    receive("/loop"+ track_id +"_pause", "0")
                    receive("/loop"+ track_id +"_mute", "0")
                    receive("/loop"+ track_id +"_trigger", "0")
                    receive("/loop"+ track_id +"_oneshot", "0")
                    receive("/loop"+ track_id +"_solo", "0")
                    receive("/loop"+ track_id +"_pos/range", {min: 0, max: 1})
                    receive("/loop"+ track_id +"_pos", 1)
                    tracks_states[track_id] = "recording"
                    break;
                case 3:		// wait stop
                    receive("/EDIT", "loop" + track_id + "_state", {"alphaFillOff": 0})
                    receive("/EDIT", "loop" + track_id + "_state", {"colorText": tracks_attrs[session_name][song_name]["tracks"][track_name]["color"]})
                    receive("/EDIT", "loop" + track_id + "_state", {"colorStroke": tracks_attrs[session_name][song_name]["tracks"][track_name]["color"]})
                    receive("/EDIT", "loop" + track_id + "_state", {"alphaStroke": 1})
                    tracks_states[track_id] = "wait stop"
                    break;
                case 4:		// playing
                    receive("/EDIT", "loop" + track_id + "_state", {"alphaFillOff": 1})
                    receive("/EDIT", "loop" + track_id + "_state", {"colorText": "#3C2013"})
                    receive("/EDIT", "loop" + track_id + "_state", {"alphaStroke": 0})
                    receive("/loop"+ track_id +"_record", "0")
                    receive("/loop"+ track_id +"_pause", "pause")
                    receive("/loop"+ track_id +"_mute", "0")
                    receive("/loop"+ track_id +"_trigger", "0")
                    receive("/loop"+ track_id +"_oneshot", "0")
                    receive("/loop"+ track_id +"_solo", "0")
                    if (tracks_states[track_id] == "wait stop") {
                        let secs = tracks_lens[track_id]
                        logDebug("secs: " + secs)
                        receive("/loop"+ track_id +"_pos/range", {min: 0, max: secs})
                        receive("/loop"+ track_id +"_len", "^clock " + Math.floor(secs/60).toString().padStart(2, '0') + ":" + (secs%60).toFixed(2).padStart(2, '0'))
                    }

                    logDebug("session_name: " + session_name)

                    if (track_id == 0) {
                        let curr_tracks_attrs = tracks_attrs[session_name][song_name]["tracks"]
                        let song_autoplay_delay = tracks_attrs[session_name][song_name]["autoplay_delay"]
                        logDebug("song_autoplay_delay: " + song_autoplay_delay)

                        Object.keys(curr_tracks_attrs).forEach((track_name, idx) => {
                            let autoplay = tracks_attrs[session_name][song_name]["tracks"][track_name]["autoplay"]
                            logDebug("track_autoplay: " + autoplay)

                            if (autoplay) {
                                let track_autoplay_delay = tracks_attrs[session_name][song_name]["tracks"][track_name]["autoplay_delay"]
                                logDebug("track_autoplay_delay: " + track_autoplay_delay)
                                let autoplay_delay = track_autoplay_delay || song_autoplay_delay || session_autoplay_delay || MIN_DELAY
                                logDebug("autoplay_delay: " + autoplay_delay)

                                if (autoplay_delay >= MIN_DELAY) {
                                    // after autoplay_delay seconds trigger also tracks that have non-false autoplay attribute
                                    setTimeout(() => {
                                        triggerClickAndSounds(idx, autoplay)
                                        }, ((tracks_lens[0] * autoplay_delay) - 0.2) * 1000)
                                }
                                else {
                                    logError("Invalid value '" + autoplay_delay + "' for track '" + track_name + "' (" + idx + ").")
                                }
                            }
                        })
                    }

                    tracks_states[track_id] = "loop"
                    break;
                case 5:		// overdubbing
                    receive("/EDIT", "loop" + track_id + "_state", {"alphaFillOff": 1})
                    receive("/EDIT", "loop" + track_id + "_state", {"colorText": "#3C2013"})
                    receive("/EDIT", "loop" + track_id + "_state", {"alphaStroke": 0})
                    receive("/loop"+ track_id +"_record", "overdub")
                    receive("/loop"+ track_id +"_pause", "0")
                    receive("/loop"+ track_id +"_mute", "0")
                    receive("/loop"+ track_id +"_oneshot", "0")
                    tracks_states[track_id] = "overdubbing"
                    break;
                case 10:	// muted
                // case 20:    // unsynced mute? Looks weird in the GUI - tracks in off state are muted immediately, playing tracks after 1 sync loop
                    receive("/EDIT", "loop" + track_id + "_state", {"alphaFillOff": 1})
                    receive("/EDIT", "loop" + track_id + "_state", {"colorText": "#3C2013"})
                    receive("/EDIT", "loop" + track_id + "_state", {"alphaStroke": 0})
                    receive("/loop"+ track_id +"_mute", "mute")
                    tracks_states[track_id] = "muted"

                    logDebug("track_id: " + track_id)
                    break;
                case 12:	// oneshot
                    receive("/EDIT", "loop" + track_id + "_state", {"alphaFillOff": 1})
                    receive("/EDIT", "loop" + track_id + "_state", {"colorText": "#3C2013"})
                    receive("/EDIT", "loop" + track_id + "_state", {"alphaStroke": 0})
                    logDebug("once: " + track_id)
                    receive("/loop"+ track_id +"_oneshot", "oneshot")
                    receive("/loop"+ track_id +"_pause", "0")
                    receive("/loop"+ track_id +"_record", "0")
                    receive("/loop"+ track_id +"_mute", "0")
                    tracks_states[track_id] = "once"
                    break; 
                case 14:	// paused
                    receive("/EDIT", "loop" + track_id + "_state", {"alphaFillOff": 1})
                    receive("/EDIT", "loop" + track_id + "_state", {"colorText": "#3C2013"})
                    receive("/EDIT", "loop" + track_id + "_state", {"alphaStroke": 0})
                    receive("/loop"+ track_id +"_record", "0")
                    receive("/loop"+ track_id +"_pause", "0")
                    receive("/loop"+ track_id +"_mute", "0")
                    receive("/loop"+ track_id +"_trigger", "0")
                    receive("/loop"+ track_id +"_oneshot", "0")
                    receive("/loop"+ track_id +"_solo", "0")
                    tracks_states[track_id] = "paused"
                    break;                                           
            }
            
            receive("/loop"+ track_id +"_state", tracks_states[track_id])
        }
        else if (address == "/loop_len") {
            let track_id = args[0].value
            let track_state = ((args.length > 2) ? args[2].value : null)

            logDebug("LEN: loop: " + track_id + ", ctrl: " + args[1].value + ", value: " + track_state + ", host: " + host + ", port: " + port)

            tracks_lens[track_id] = track_state
            logDebug("track_state: " + track_state)
            if (tracks_states[track_id] != "recording") {
                let secs = tracks_lens[track_id]
                logDebug("secs: " + secs)
                receive("/loop"+ track_id +"_pos/range", {min: 0, max: secs})
                receive("/loop"+ track_id +"_len", "^clock " + Math.floor(secs/60).toString().padStart(2, '0') + ":" + (secs%60).toFixed(2).padStart(2, '0'))
            }
        }
        else if (address == "/loop_pos") {
            let track_id = args[0].value
            let track_state = ((args.length > 2) ? args[2].value : null)

            if (tracks_states[track_id] != "recording") {
                address = "/loop" + track_id
                receive(address + "_pos", track_state)
            }
        }
        else if (address == "/loop_vol") {
            let track_id = args[0].value
            let track_state = ((args.length > 2) ? args[2].value : null)

            logDebug("VOLUME: loop: " + track_id + ", ctrl: " + args[1].value + ", value: " + track_state + ", host: " + host + ", port: " + port)
            receive("/loop"+ track_id+"_vol", track_state)
        }
        else if (address == "/loop_selected") {
            let track_id = args[0].value
            let track_state = ((args.length > 2) ? args[2].value : null)
            
            logDebug("SELECTED: loop: " + track_id + ", ctrl: " + args[1].value + ", value: " + track_state + ", host: " + host + ", port: " + port)
            receive("/EDIT", "tracks/" + selected_track, {"css": "background: none"})
            receive("/EDIT", "tracks/" + track_state, {"css": "background: #deaa88; border-radius: 5rem"})
            selected_track = track_state
        }
        else if (address == "/session") {
            session_name = args[0].value
            if (args[1])
                session_label = args[1].value
            
            logDebug("session_name: " + session_name)
            logDebug("session_label: " + session_label)

            receive("/EDIT", "session", {"value": (session_label ? session_label : session_name)})

            let song_path = sessions_dir + "/" + session_name + "/" + session_name + "_??_*.slsess"
            logDebug("song_path: " + song_path)
            const get_songs_cnt = spawn("ls " + song_path + "|wc -w", {shell: true})
            get_songs_cnt.stdout.on('data', (data) => {
                songs_cnt = parseInt(data)

                for (i = 1; i < 9 + 1; i++) {
                    receive("/EDIT", "song_" + i, {"alphaFillOff": 0})
                    receive("/EDIT", "song_" + i, {"alphaFillOn": 0})
                    receive("/EDIT", "song_" + i, {"interaction": false})
                }

                for (i = 1; i < songs_cnt + 1; i++) {
                    receive("/EDIT", "song_" + i, {"alphaFillOff": 0.5})
                    receive("/EDIT", "song_" + i, {"alphaFillOn": 1})
                    receive("/EDIT", "song_" + i, {"interaction": true})
                }
            })
        }
        else if (address == "/song") {
            song_name = args[0].value
            if (args[1])
                song_label = args[1].value

            logDebug("song_name: " + song_name)
            logDebug("song_label: " + song_label)

            send(sl_host, sl_port, "/sl/-1/hit undo_all")
            receive("/EDIT", "loading", {"visible": true})
            receive("/EDIT", "tracks", {"visible": false})

            receive("/EDIT", "song", {"value": (song_label ? song_label : song_name)})
            receive("/EDIT", "song_" + (song_id + 1), {"colorWidget": "#deaa88"})   // current button

            song_id = (song_name.split("_")[1] * 1) - 1
            logDebug("song_id: " + song_id)
            
            receive("/EDIT", "song_" + (song_id + 1), {"colorWidget": "red"})   // next button

            let songs_dir = sessions_dir + "/" + song_name.split("_")[0]
            logDebug("songs_dir: " + songs_dir)
            let song_path = songs_dir + "/" + song_name + ".slsess"
            logDebug("song_path: " + song_path)

            if (tracks_cnt) {
                // unregister previous general updates
                send(sl_host, sl_port, '/unregister_update', 'selected_loop_num', osc_host, '/loop_selected')
                
                // unregister previous track updates
                let tracks_range = "[0-" + (tracks_cnt - 1) + "]"
                send(sl_host, sl_port, '/sl/' + tracks_range + '/unregister_update', 'wet', osc_host, '/loop_vol')
                            
                send(sl_host, sl_port, '/sl/' + tracks_range + '/unregister_auto_update', 'state', osc_host, '/loop_state')        
                send(sl_host, sl_port, '/sl/' + tracks_range + '/unregister_auto_update', 'loop_len', osc_host, '/loop_len')        
                send(sl_host, sl_port, '/sl/' + tracks_range + '/unregister_auto_update', 'loop_pos', osc_host, '/loop_pos')
            }

            const get_tracks_cnt = spawn("grep \"index=\" " + song_path + "|wc -l", {shell: true})
            get_tracks_cnt.stdout.on('data', (data) => {
                tracks_cnt = parseInt(data)

                // first create tracks
                receive("/EDIT", "tracks", {"quantity": tracks_cnt})
                receive("/EDIT", "tracks", {"height": tracks_cnt * TRACK_HEIGHT})

                // setting up specific tracks must take place
                // after a delay, when the tracks are already created
                logDebug("song_name: " + song_name)
                setUpTracks(tracks_cnt, song_name, song_path)
            })            
            // return {address, args, host, port}
            }
        else if (address == "/EDIT") {
            // just so it's not caught by catch all else below
            // and log doesn't get cluttered by debug prints
            return {address, args, host, port}
        }
        else {
            logDebug("OTHER: ")
            args.forEach(logDebug)
            return {address, args, host, port}
        }     
    },

    oscOutFilter: function(data) {
        let {address, args, host, port, clientId} = data

        let track_id = args[0].value

        if (address.match("^/loop.?_record|^/loop.?_pause|^/loop.?_mute|^/loop.?_trigger|^/loop.?_oneshot|^/loop.?_solo")) {
            logDebug("value: " + track_id)
            logDebug("loop #: " + address[5])
            args[0] = {type: "s", value: address.split("_")[1]}
            address = "/sl/"+ address[5] +"/hit"
            
            logDebug("session_name: " + session_name)
            logDebug("value: " + args[0].value)
        }
        else if (address.match("^/session_prev|^/session_next")) {
            let operation = address.split("_")[1] + "s"
            logDebug(operation)
            send(sh_host, sh_port, "/" + operation)
        }
        else if (address.match("^/song_prev|^/song_next|^/song_?")) {
            let operation = address.split("_")[1]
            logDebug(operation)
            send(sh_host, sh_port, "/" + operation)
        }
        else if (address.match("^/export")) {
            logDebug("exporting")

            let today = new Date();
            let day = String(today.getDate()).padStart(2, '0');
            let month = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            let year = today.getFullYear();
            let hour = String(today.getHours()).padStart(2, '0');
            let min = String(today.getMinutes()).padStart(2, '0');
            let sec = String(today.getSeconds()).padStart(2, '0');
            today = year + month + day + "_" + hour + min + sec;

            const src_dir = recorder_dir
            const dst_dir = recorder_dir_tracks_save + '/recording_stems_' + today + "/"
                        
            fs.exists((dst_dir), exists => {
                if (!exists) {
                    fs.mkdirSync(dst_dir, (err) => {
                        if (err) 
                            throw err;
                    });
                }
            });

            fs.readdir(src_dir, (err, files) => {
                files.forEach(file => {
                    logDebug(file);
                    fs.copyFile(src_dir + file, dst_dir + file, (err) => {
                        if (err)
                            throw err
                    })
                });
            });             
        }        
        
        return {address, args, host, port}
    },

    unload: function() {
        // this will be executed when the custom module is reloaded
    },

}
