import pytest
import os
import shutil
import yaml
import psutil
import time
import jack
import rtmidi
from pathlib import Path
from subprocess import Popen, DEVNULL, PIPE
from studiosh import StudioSh

APP_DIR=os.getcwd()
TMP_DIR = "/tmp/studiosh_test/"
CONF_DIR = f"{TMP_DIR}config/"
SESS_DIR = f"{TMP_DIR}sessions/"
SESS_FILE_EXTENSION = ".slsess"
INIT_SESS = "no session"
INIT_SONG = "no song"

@pytest.fixture
def create_root():
    os.makedirs(CONF_DIR, exist_ok = True)
    shutil.copy("./studiosh.py", TMP_DIR)
    shutil.copy("./studiosh", TMP_DIR)

    yield True

    os.remove(f"{TMP_DIR}/studiosh")
    os.remove(f"{TMP_DIR}/studiosh.py")
    os.removedirs(CONF_DIR)

@pytest.fixture
def open_configuration(request):
    configuration = None
    conf_version = request.param

    shutil.copy(f"./tests/assets/configs/studiosh_{conf_version}.yaml", f"{CONF_DIR}studiosh.yaml")
    shutil.copy(f"./tests/assets/configs/studiosh_gui.yaml", f"{CONF_DIR}studiosh_gui.yaml")
    
    with open(f"{CONF_DIR}studiosh.yaml", "r") as config:
        try:
            configuration = yaml.safe_load(config)
        except yaml.YAMLError:
            print("Error: Failed loading yaml configuration.")

    yield configuration

    os.remove(f"{CONF_DIR}/studiosh.yaml")
    os.remove(f"{CONF_DIR}/studiosh_gui.yaml")

def createSession(session, songs):
    os.makedirs(SESS_DIR + session, exist_ok = True)
    
    f = Path(SESS_DIR + session + "/" + session + "_aux" + SESS_FILE_EXTENSION)
    f.touch()

    if songs:
        for song in songs:
            f = Path(SESS_DIR + session + "/" + song + SESS_FILE_EXTENSION)
            f.touch()


def removeSession(session, songs):
    if songs:
        for song in songs:
            if os.path.isfile(SESS_DIR + session + "/" + song + SESS_FILE_EXTENSION):
                os.remove(SESS_DIR + session + "/" + song + SESS_FILE_EXTENSION)
    
    if os.path.isfile(SESS_DIR + session + "/" + session + "_aux" + SESS_FILE_EXTENSION):
        os.remove(SESS_DIR + session + "/" + session + "_aux" + SESS_FILE_EXTENSION)
    
    if os.path.isdir(SESS_DIR + session):
        os.rmdir(SESS_DIR + session)

@pytest.fixture
def setupConfiguration(create_root, open_configuration):
    if open_configuration:
        os.makedirs(SESS_DIR, exist_ok = True)

        if "sessions" in open_configuration:
            if isinstance(open_configuration["sessions"], dict):
                sessions = open_configuration["sessions"].keys()
                for session in sessions:
                    songs = open_configuration["sessions"][session]
                    createSession(session, songs)
                    os.chdir(TMP_DIR)

    yield open_configuration

    os.chdir(APP_DIR)

    if open_configuration:
        if "sessions" in open_configuration:
            if isinstance(open_configuration["sessions"], dict):
                sessions = open_configuration["sessions"].keys()
                for session in sessions:
                    songs = open_configuration["sessions"][session]
                    removeSession(session, songs)

        os.removedirs(SESS_DIR)

# creating studiosh object
def test_creating_with_config_file_shouldnt_print_missing_config_file_error(capfd):
    studio_sh = StudioSh()
    out, err = capfd.readouterr()
    assert not err == f"Error: Missing configuration file in '{TMP_DIR}config/'.\n"

def test_creating_studiosh_without_args_should_set_no_session_no_song():
    studio_sh = StudioSh()
    assert studio_sh is not None
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG

# starting studiosh object
def test_start_with_too_many_arguments_should_print_too_many_arguments_error(capfd):
    studio_sh = StudioSh()
    studio_sh.do_start("foo bar baz")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: Too many arguments.\n"
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_start_already_running_session_should_do_nothing(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    ret = studio_sh.do_start("")
    assert ret == None
    ret = studio_sh.do_start("")
    assert ret == False

@pytest.mark.parametrize("open_configuration", ["empty"], indirect = True)
def test_start_with_empty_config_should_print_no_session_found_error(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: No session found in configuration file.\n"
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG 

@pytest.mark.parametrize("open_configuration", ["empty"], indirect = True)
def test_start_baz_with_empty_config_should_print_no_session_found_error(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("baz")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: No session found in configuration file.\n"
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG   

@pytest.mark.parametrize("open_configuration", ["no_sessions"], indirect = True)
def test_start_with_no_session_in_config_should_print_no_session_found_error(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: No session found in configuration file.\n"
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG 

@pytest.mark.parametrize("open_configuration", ["no_songs"], indirect = True)
def test_start_with_session_without_songs_should_print_session_has_no_songs_error(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: Session 'foo' has no songs.\n"
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG 

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_start_with_nonexistent_session_should_print_error(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("abc")
    out, err = capfd.readouterr()
    assert err == "Error: Session 'abc' has no songs.\n"
    
@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_start_should_set_foo(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("")
    assert studio_sh is not None
    assert studio_sh.session_name == "foo"
    assert studio_sh.song_name == "foo_01_song"

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_start_foo_should_set_foo_01_song(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("foo")
    assert studio_sh is not None
    assert studio_sh.session_name == "foo"
    assert studio_sh.song_name == "foo_01_song"

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_start_baz_should_set_baz_01_song(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("baz")
    assert studio_sh is not None
    assert studio_sh.session_name == "baz"
    assert studio_sh.song_name == "baz_01_song"

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_start_3_should_set_foo_03_song(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("3")
    assert studio_sh is not None
    assert studio_sh.session_name == "foo"
    assert studio_sh.song_name == "foo_03_song"

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_start_baz_2_should_set_baz_02_song(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("baz 2")
    assert studio_sh is not None
    assert studio_sh.session_name == "baz"
    assert studio_sh.song_name == "baz_02_song"

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_start_baz_too_large_song_number_should_print_incorrect_song_number_error(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("baz 12")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: Incorrect song number for session 'baz': 12.\n"    
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_sta_should_set_foo(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_sta("")
    assert studio_sh is not None
    assert studio_sh.session_name == "foo"
    assert studio_sh.song_name == "foo_01_song"

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_start_0_song_number_should_print_incorrect_song_number_error(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("0")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: Incorrect song number for session 'foo': 0.\n"
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_start_too_small_song_number_should_print_incorrect_song_number_error(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("-5")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: Incorrect song number for session 'foo': -5.\n"
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG   

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_start_foo_too_small_song_number_should_print_incorrect_song_number_error(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("foo -3")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: Incorrect song number for session 'foo': -3.\n"
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_start_too_large_song_number_should_print_incorrect_song_number_error(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("31")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: Incorrect song number for session 'foo': 31.\n"
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG    

# starting studiosh object with session names
@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_foo_should_set_foo_01_song(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.default("foo")
    assert studio_sh is not None
    assert studio_sh.session_name == "foo"
    assert studio_sh.song_name == "foo_01_song"

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_foo_and_foo_3_should_set_foo_03_song(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.default("foo")
    studio_sh.default("foo 3")
    assert studio_sh is not None
    assert studio_sh.session_name == "foo"
    assert studio_sh.song_name == "foo_03_song"

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_foo_and_baz_2_should_set_baz_02_song(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.default("foo")
    studio_sh.default("baz 2")
    assert studio_sh is not None
    assert studio_sh.session_name == "baz"
    assert studio_sh.song_name == "baz_02_song"

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_foo_too_large_song_number_should_print_incorrect_song_number_error(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.default("foo 5")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: Incorrect song number for session 'foo': 5.\n"
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG    

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_baz_too_large_song_song_number_should_print_incorrect_song_number_error(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.default("baz 10")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: Incorrect song number for session 'baz': 10.\n"
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_baz_should_set_baz_01_song(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.default("baz")
    assert studio_sh is not None
    assert studio_sh.session_name == "baz"
    assert studio_sh.song_name == "baz_01_song"
    
@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_too_small_song_number_should_print_incorrect_song_number_error(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.default("-8")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: Incorrect song number for session 'foo': -8.\n"
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG
    
@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_nonexistend_session_name_should_print_session_does_not_exist_error(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.default("band")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: Unrecognized command: 'band'.\n"
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG

# starting studiosh object with song number
@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_10_should_print_incorrect_song_number_error(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.default("10")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: Incorrect song number for session 'foo': 10.\n"
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG

# stopping studiosh object 
def test_stop_should_set_no_session_no_song():
    studio_sh = StudioSh()
    studio_sh.do_start("")
    studio_sh.do_stop("")
    assert studio_sh is not None
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG

def test_sto_should_set_no_session_no_song():
    studio_sh = StudioSh()
    studio_sh.do_start("")
    studio_sh.do_sto("")
    assert studio_sh is not None
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG    


# restarting studiosh object 
@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_restart_should_set_foo_01_song(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("")
    studio_sh.do_restart("")
    assert studio_sh is not None
    assert studio_sh.session_name == "foo"
    assert studio_sh.song_name == "foo_01_song"

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_restart_without_argument_should_restart_to_current_session(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("baz")
    studio_sh.do_restart("")
    assert studio_sh is not None
    assert studio_sh.session_name == "baz"
    assert studio_sh.song_name == "baz_01_song"

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_restart_without_argument_should_restart_to_current_session_and_song(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("baz 2")
    studio_sh.do_restart("")
    assert studio_sh is not None
    assert studio_sh.session_name == "baz"
    assert studio_sh.song_name == "baz_02_song"

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_restart_too_large_song_should_print_incorrect_song_number_error(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("")
    studio_sh.do_restart("baz 7")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: Incorrect song number for session 'baz': 7.\n"    
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_restart_too_small_song_should_print_incorrect_song_number_error(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("")
    studio_sh.do_restart("baz -7")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: Incorrect song number for session 'baz': -7.\n"    
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_res_should_set_foo_01_song(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("")
    studio_sh.do_res("")
    assert studio_sh is not None
    assert studio_sh.session_name == "foo"
    assert studio_sh.song_name == "foo_01_song"

# sessions & songs info
# session
def test_session_before_start_should_print_no_session_error(capfd):
    studio_sh = StudioSh()
    studio_sh.do_session("")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: No session running. Run start first.\n" 

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_session_from_default_session_should_print_foo(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("")
    studio_sh.do_session("")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert out == "session: foo.\n"

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_session_from_baz_should_print_baz(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("baz")
    studio_sh.do_session("")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert out == "session: baz.\n"

# sessions
@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_sessions_from_initial_session_should_print_all_sessions(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_sessions("")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert out == "sessions (3): foo, bar, baz.\n" 

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_sessions_from_started_session_should_print_all_sessions(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("")
    studio_sh.do_sessions("")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert out == "sessions (3): foo, bar, baz.\n" 

# song
def test_song_before_start_should_print_no_session_error(capfd):
    studio_sh = StudioSh()
    studio_sh.do_song("")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: No session running. Run start first.\n" 

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_song_from_baz_session_should_print_first_baz_song(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("baz")
    studio_sh.do_song("")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert out == "song: baz_01_song.\n"

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_song_from_default_session_should_print_first_foo_song(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("")
    studio_sh.do_song("")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert out == "song: foo_01_song.\n"

# songs
def test_songs_from_initial_session_should_print_no_session_running_error(capfd):
    studio_sh = StudioSh()
    studio_sh.do_songs("")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: No session running. Run start first.\n" 

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_songs_from_foo_session_should_print_all_songs(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("")
    studio_sh.do_songs("")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert out == "songs (3): foo_01_song, foo_02_song, foo_03_song.\n" 

# changing session
def test_nexts_from_initial_session_should_print_no_session_running_error(capfd):
    studio_sh = StudioSh()
    studio_sh.do_nexts("")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: No session running. Run start first.\n" 
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_nexts_from_first_session_should_change_to_second_session(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("")
    studio_sh.do_nexts("")
    assert studio_sh is not None
    assert studio_sh.session_name == "bar"
    assert studio_sh.song_name == "bar_01_song"    

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_next_and_nexts_from_first_session_should_change_to_second_session_first_song(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("")
    studio_sh.do_next("")
    studio_sh.do_nexts("")
    assert studio_sh is not None
    assert studio_sh.session_name == "bar"
    assert studio_sh.song_name == "bar_01_song"    

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_ns_from_first_session_should_change_to_second_session(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("")
    studio_sh.do_ns("")
    assert studio_sh is not None
    assert studio_sh.session_name == "bar"
    assert studio_sh.song_name == "bar_01_song"    

def test_prevs_from_initial_session_should_print_no_session_running_error(capfd):
    studio_sh = StudioSh()
    studio_sh.do_prevs("")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: No session running. Run start first.\n" 
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_prevs_from_first_session_should_change_to_last_session(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("")
    studio_sh.do_prevs("")
    assert studio_sh is not None
    assert studio_sh.session_name == "baz"
    assert studio_sh.song_name == "baz_01_song"    

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_prev_and_prevs_from_first_session_should_change_to_last_session_first_song(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("")
    studio_sh.do_prev("")
    studio_sh.do_prevs("")
    assert studio_sh is not None
    assert studio_sh.session_name == "baz"
    assert studio_sh.song_name == "baz_01_song"    

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_ps_from_first_session_should_change_to_last_session(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("")
    studio_sh.do_ps("")
    assert studio_sh is not None
    assert studio_sh.session_name == "baz"
    assert studio_sh.song_name == "baz_01_song"    

@pytest.mark.parametrize("open_configuration", ["full"], indirect = True)
def test_baz_from_foo_session_should_change_to_baz_not_start_it(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("foo")
    out, err = capfd.readouterr()
    studio_sh.default("baz")
    out, err = capfd.readouterr()
    studio_sh.do_stop("")
    assert "Starting program" not in out

# changing song
def test_next_from_initial_session_should_print_no_session_running_error(capfd):
    studio_sh = StudioSh()
    studio_sh.do_next("")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: No session running. Run start first.\n" 
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_next_from_song_1_should_change_to_song_2(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("")
    studio_sh.do_next("")
    assert studio_sh is not None
    assert studio_sh.session_name == "foo"
    assert studio_sh.song_name == "foo_02_song"

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_next_from_last_song_should_change_to_first_song(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("3")
    studio_sh.do_next("")
    assert studio_sh is not None
    assert studio_sh.session_name == "foo"
    assert studio_sh.song_name == "foo_01_song"

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_n_from_last_song_should_change_to_first_song(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("3")
    studio_sh.do_n("")
    assert studio_sh is not None
    assert studio_sh.session_name == "foo"
    assert studio_sh.song_name == "foo_01_song"

def test_prev_from_initial_session_should_print_no_session_running_error(capfd):
    studio_sh = StudioSh()
    studio_sh.do_prev("")
    out, err = capfd.readouterr()
    assert studio_sh is not None
    assert err == "Error: No session running. Run start first.\n" 
    assert studio_sh.session_name == INIT_SESS
    assert studio_sh.song_name == INIT_SONG

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_prev_from_song_3_should_change_to_song_2(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("3")
    studio_sh.do_prev("")
    assert studio_sh is not None
    assert studio_sh.session_name == "foo"
    assert studio_sh.song_name == "foo_02_song"

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_p_from_song_3_should_change_to_song_2(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("3")
    studio_sh.do_p("")
    assert studio_sh is not None
    assert studio_sh.session_name == "foo"
    assert studio_sh.song_name == "foo_02_song"

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_prev_from_first_song_should_change_to_last_song(setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("")
    studio_sh.do_prev("")
    assert studio_sh is not None
    assert studio_sh.session_name == "foo"
    assert studio_sh.song_name == "foo_03_song"    

# quitting
def test_quit(capfd):
    studio_sh = StudioSh()
    studio_sh.do_start("")
    studio_sh.do_quit("")
    out, err = capfd.readouterr()
    assert out == "Adios!\n"

def test_q(capfd):
    studio_sh = StudioSh()
    studio_sh.do_start("")
    studio_sh.do_q("")
    out, err = capfd.readouterr()
    assert out == "Adios!\n"

def test_exit(capfd):
    studio_sh = StudioSh()
    studio_sh.do_start("")
    studio_sh.do_exit("")
    out, err = capfd.readouterr()
    assert out == "Adios!\n"

def test_e(capfd):
    studio_sh = StudioSh()
    studio_sh.do_start("")
    studio_sh.do_e("")
    out, err = capfd.readouterr()
    assert out == "Adios!\n"

def test_bye(capfd):
    studio_sh = StudioSh()
    studio_sh.do_start("")
    studio_sh.do_bye("")
    out, err = capfd.readouterr()
    assert out == "Adios!\n"

def test_b(capfd):
    studio_sh = StudioSh()
    studio_sh.do_start("")
    studio_sh.do_b("")
    out, err = capfd.readouterr()
    assert out == "Adios!\n"

def test_adios(capfd):
    studio_sh = StudioSh()
    studio_sh.do_start("")
    studio_sh.do_adios("")
    out, err = capfd.readouterr()
    assert out == "Adios!\n"

def test_a(capfd):
    studio_sh = StudioSh()
    studio_sh.do_start("")
    studio_sh.do_a("")
    out, err = capfd.readouterr()
    assert out == "Adios!\n"

# starting app with and without config
@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_creating_without_config_file_should_print_missing_config_file_error(capfd, setupConfiguration):
    cwd = os.getcwd()    
    os.rename(f"{CONF_DIR}studiosh.yaml", f"{CONF_DIR}studiosh.yaml.deleted")
    os.chdir(TMP_DIR)    
    os.system("./studiosh")
    os.rename(f"{CONF_DIR}studiosh.yaml.deleted", f"{CONF_DIR}studiosh.yaml")    
    os.chdir(cwd)    
    out, err = capfd.readouterr()
    assert err == f"Error: Missing configuration file in './config/'. Exiting.\n"

@pytest.mark.parametrize("open_configuration", ["garbage"], indirect = True)
def test_creating_with_garbage_config_file_should_print_invalid_config_file_error(capfd, setupConfiguration):
    cwd = os.getcwd()    
    os.chdir(TMP_DIR)    
    os.system("./studiosh")
    os.chdir(cwd)    
    out, err = capfd.readouterr()
    assert err == f"Error: Invalid configuration file in './config/': mapping values are not allowed here\n  in \"./config/studiosh.yaml\", line 2, column 10. Exiting.\n"

@pytest.mark.parametrize("open_configuration", ["foo_baz_bar"], indirect = True)
def test_creating_with_valid_config_file_shouldnt_print_any_error(capfd, setupConfiguration):
    cwd = os.getcwd()
    os.chdir(TMP_DIR)
    os.system("./studiosh")
    os.chdir(cwd)
    out, err = capfd.readouterr()
    assert not err

# stopping & starting other programs
def processNameIsRunning(process_name):
    for process in psutil.process_iter():
        if process_name.lower() in process.name().lower():
            return True
    
    return False

@pytest.mark.parametrize("open_configuration", ["programs"], indirect = True)
def test_starting_and_stopping_with_valid_programs_in_configuration_should_start_and_stop_them(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    
    studio_sh.do_start("")
    time.sleep(1)
    out, err = capfd.readouterr()
    assert not err
    assert out == "Starting session: foo, song: foo_01_song\nStarting program: a2jmidid -e\nStarting program: sooperlooper\nStarting program: jalv.gtk https://surge-synthesizer.github.io/lv2/surge-xt -n surge-xt\n"
    assert processNameIsRunning("a2jmidid")
    assert processNameIsRunning("sooperlooper")
    assert processNameIsRunning("jalv.gtk")
    
    studio_sh.do_stop("")
    time.sleep(1)
    out, err = capfd.readouterr()
    assert not err
    assert out == "Stopping session: foo, song: foo_01_song\nStopping program: jalv.gtk https://surge-synthesizer.github.io/lv2/surge-xt -n surge-xt\nStopping program: sooperlooper\nStopping program: a2jmidid -e\n"
    assert not processNameIsRunning("a2jmidid")
    assert not processNameIsRunning("sooperlooper")
    assert not processNameIsRunning("jalv.gtk")

@pytest.mark.parametrize("open_configuration", ["programs_invalid"], indirect = True)
def test_starting_and_stopping_with_invalid_programs_in_configuration_shouldnt_start_nor_stop_them(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    
    studio_sh.do_start("")
    time.sleep(1)
    out, err = capfd.readouterr()
    assert out == "Starting session: foo, song: foo_01_song\nStarting program: jackd -dalsa -dhw:K6 -r44100 -p128 -n3\nStarting program: a2jmidid -e\nStarting program: invalid_program\nStarting program: sooperlooper -invalid\nStarting program: sooperlooper\n"    
    assert err == "Error: 'jackd': No such file or directory\nError: 'invalid_program': No such file or directory\nError: 'sooperlooper': Returned error code: 1\n"
    assert not processNameIsRunning("jackd")
    assert processNameIsRunning("a2jmidid")
    assert not processNameIsRunning("invalid_program")
    assert not processNameIsRunning("sooperlooper -invalid")
    assert processNameIsRunning("sooperlooper")
    
    studio_sh.do_stop("")
    time.sleep(1)
    out, err = capfd.readouterr()
    assert not err
    assert out == """Stopping session: foo, song: foo_01_song\nStopping program: sooperlooper\nStopping program: a2jmidid -e\n"""
    assert not processNameIsRunning("a2jmidid")
    assert not processNameIsRunning("invalid_program")
    assert not processNameIsRunning("sooperlooper -invalid")    
    assert not processNameIsRunning("sooperlooper")    

@pytest.mark.parametrize("open_configuration", ["programs"], indirect = True)
def test_starting_and_quiting_without_stopping_programs_in_configuration_should_stop_them(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    
    studio_sh.do_start("")
    time.sleep(1)
    out, err = capfd.readouterr()
    assert not err
    assert out == "Starting session: foo, song: foo_01_song\nStarting program: a2jmidid -e\nStarting program: sooperlooper\nStarting program: jalv.gtk https://surge-synthesizer.github.io/lv2/surge-xt -n surge-xt\n"
    assert processNameIsRunning("a2jmidid")
    assert processNameIsRunning("sooperlooper")
    assert processNameIsRunning("jalv.gtk")    
    
    studio_sh.do_quit("")
    time.sleep(1)
    out, err = capfd.readouterr()
    assert not err
    assert out == "Stopping session: foo, song: foo_01_song\nStopping program: jalv.gtk https://surge-synthesizer.github.io/lv2/surge-xt -n surge-xt\nStopping program: sooperlooper\nStopping program: a2jmidid -e\nAdios!\n"
    assert not processNameIsRunning("a2jmidid")
    assert not processNameIsRunning("sooperlooper")
    assert not processNameIsRunning("jalv.gtk")    

@pytest.mark.parametrize("open_configuration", ["no_programs"], indirect = True)
def test_connections_without_start_should_print_error(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    
    studio_sh.do_connections("")
    out, err = capfd.readouterr()
    assert err == "Error: No session running. Run start first.\n"
    assert not out

@pytest.mark.parametrize("open_configuration", ["no_programs"], indirect = True)
def test_connections_with_incorrect_program_names_should_handle_that(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)

    studio_sh.do_start("")

    studio_sh.do_connections("")
    out, err = capfd.readouterr()
    assert err == "Error: Missing argumment: list of program names.\n"

    studio_sh.do_connections("abc")
    out, err = capfd.readouterr()
    assert not err
    assert out == "ConnectionsToMake: {\n    # abc\n    }\n"

    studio_sh.do_connections("abc xyz")
    out, err = capfd.readouterr()
    assert not err
    assert out == "ConnectionsToMake: {\n    # abc\n    # xyz\n    }\n"

test_output = """\
Starting session: foo, song: foo_01_song
ConnectionsToMake: {
    # sooperlooper
    'Comet Lake PCH-LP cAVS Digital Microphone:capture_FL': 'sooperlooper:loop0_in_1',
    'Comet Lake PCH-LP cAVS Digital Microphone:capture_FR': 'sooperlooper:loop0_in_2',
    'sooperlooper:loop0_out_1': 'Comet Lake PCH-LP cAVS Speaker + Headphones:playback_FL',
    'sooperlooper:loop0_out_2': 'Comet Lake PCH-LP cAVS Speaker + Headphones:playback_FR',
    }\n"""
@pytest.mark.parametrize("open_configuration", ["no_programs"], indirect = True)
def test_connections_should_print_all_program_connections(capfd, setupConfiguration):
    p = Popen(["sooperlooper"], stdout = DEVNULL, stderr = DEVNULL)
    time.sleep(1)

    jc = jack.Client("tester")
    time.sleep(0.5)
    jc.connect('Comet Lake PCH-LP cAVS Digital Microphone:capture_FL', 'sooperlooper:loop0_in_1')
    jc.connect('Comet Lake PCH-LP cAVS Digital Microphone:capture_FR', 'sooperlooper:loop0_in_2')
    jc.connect('sooperlooper:loop0_out_1', 'Comet Lake PCH-LP cAVS Speaker + Headphones:playback_FL')
    jc.connect('sooperlooper:loop0_out_2', 'Comet Lake PCH-LP cAVS Speaker + Headphones:playback_FR')

    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("")

    studio_sh.do_connections("sooperlooper")
    out, err = capfd.readouterr()
    p.kill()
    p.communicate()
    assert not err
    assert out == test_output

test_output2 = """\
Starting session: baz, song: baz_01_song
Starting program: sooperlooper
Starting program: jalv.gtk https://surge-synthesizer.github.io/lv2/surge-xt -n surge-xt
Starting program: qjackctl
Creating connection from 'Comet Lake PCH-LP cAVS Digital Microphone:capture_FL' to 'sooperlooper:loop0_in_1'.
Creating connection from 'Comet Lake PCH-LP cAVS Digital Microphone:capture_FR' to 'sooperlooper:loop0_in_2'.
Creating connection from 'sooperlooper:loop0_out_1' to 'Comet Lake PCH-LP cAVS Speaker + Headphones:playback_FL'.
Creating connection from 'sooperlooper:loop0_out_2' to 'Comet Lake PCH-LP cAVS Speaker + Headphones:playback_FR'.
Creating connection from 'surge-xt:lv2_audio_out_1' to 'Comet Lake PCH-LP cAVS Speaker + Headphones:playback_FL'.
Creating connection from 'surge-xt:lv2_audio_out_2' to 'Comet Lake PCH-LP cAVS Speaker + Headphones:playback_FR'.
Creating connection from 'Comet Lake PCH-LP cAVS Digital Microphone:capture_FL' to 'surge-xt:lv2_audio_in_1'.
Creating connection from 'Comet Lake PCH-LP cAVS Digital Microphone:capture_FR' to 'surge-xt:lv2_audio_in_2'.\n"""
@pytest.mark.parametrize("open_configuration", ["connections"], indirect = True)
def test_connecting_with_valid_programs_in_configuration_should_connect_them(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)

    studio_sh.do_start("baz")
    out, err = capfd.readouterr()
    studio_sh.do_stop("")
    assert not err
    assert out == test_output2
    assert not processNameIsRunning("sooperlooper")

@pytest.mark.parametrize("open_configuration", ["connections_programs_missing"], indirect = True)
def test_connecting_with_missing_programs_in_configuration_should_print_error(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)

    studio_sh.do_start("")
    out, err = capfd.readouterr()
    studio_sh.do_stop("")
    assert out == "Starting session: foo, song: foo_01_song\nCreating connection from 'Comet Lake PCH-LP cAVS Digital Microphone:capture_FL' to 'sooperlooper:loop0_in_1'.\nCreating connection from 'sooperlooper:loop0_out_1' to 'Comet Lake PCH-LP cAVS Speaker + Headphones:playback_FL'.\n"
    assert err == "Error: Failed to connect 'Comet Lake PCH-LP cAVS Digital Microphone:capture_FL' to 'sooperlooper:loop0_in_1'.\nError: Failed to connect 'sooperlooper:loop0_out_1' to 'Comet Lake PCH-LP cAVS Speaker + Headphones:playback_FL'.\n"

test_output_labels = """\
Starting session: baz, song: baz_01_song
Calling hook 'oscHook1' sending OSC message '/session baz' to port '8888'
Calling hook 'oscHook2' sending OSC message '/song baz_01_song' to port '8888'
Calling hook 'execHook1' running command 'echo baz ""'
baz 
Calling hook 'execHook2' running command 'echo baz_01_song ""'
baz_01_song 
Stopping session: baz, song: baz_01_song\n"""
@pytest.mark.parametrize("open_configuration", ["labels"], indirect = True)
def test_configuration_without_session_label_shouldnt_print_it(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)

    studio_sh.do_start("baz")
    studio_sh.do_stop("")
    out, err = capfd.readouterr()
    assert out == test_output_labels

test_output_labels2 = """\
Starting session: Bar Band (bar), song: bar_01_song
Calling hook 'oscHook1' sending OSC message '/session bar Bar Band' to port '8888'
Calling hook 'oscHook2' sending OSC message '/song bar_01_song' to port '8888'
Calling hook 'execHook1' running command 'echo bar "Bar Band"'
bar Bar Band
Calling hook 'execHook2' running command 'echo bar_01_song ""'
bar_01_song 
Stopping session: Bar Band (bar), song: bar_01_song\n"""
@pytest.mark.parametrize("open_configuration", ["labels"], indirect = True)
def test_configuration_with_session_label_should_print_it(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)

    studio_sh.do_start("bar")
    studio_sh.do_stop("")
    out, err = capfd.readouterr()
    assert out == test_output_labels2

test_output_labels3 = """\
Starting session: qux, song: Qux Qux Qux (qux_01_song)
Calling hook 'oscHook1' sending OSC message '/session qux' to port '8888'
Calling hook 'oscHook2' sending OSC message '/song qux_01_song Qux Qux Qux' to port '8888'
Calling hook 'execHook1' running command 'echo qux ""'
qux 
Calling hook 'execHook2' running command 'echo qux_01_song "Qux Qux Qux"'
qux_01_song Qux Qux Qux
Stopping session: qux, song: Qux Qux Qux (qux_01_song)\n"""
@pytest.mark.parametrize("open_configuration", ["labels"], indirect = True)
def test_configuration_without_song_label_shouldnt_print_it(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)

    studio_sh.do_start("qux")
    studio_sh.do_stop("")
    out, err = capfd.readouterr()
    assert out == test_output_labels3

test_output_labels4 = """\
Starting session: Foo Fighters (foo), song: Fighting Foo! (foo_01_song)
Calling hook 'oscHook1' sending OSC message '/session foo Foo Fighters' to port '8888'
Calling hook 'oscHook2' sending OSC message '/song foo_01_song Fighting Foo!' to port '8888'
Calling hook 'execHook1' running command 'echo foo "Foo Fighters"'
foo Foo Fighters
Calling hook 'execHook2' running command 'echo foo_01_song "Fighting Foo!"'
foo_01_song Fighting Foo!
Calling hook 'oscSongChange' sending OSC message '/song foo_01_song Fighting Foo!' to port '8888'
Stopping session: Foo Fighters (foo), song: Fighting Foo! (foo_01_song)\n"""
@pytest.mark.parametrize("open_configuration", ["labels"], indirect = True)
def test_configuration_with_song_label_should_print_it(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)

    studio_sh.do_start("foo")
    studio_sh.do_stop("")
    out, err = capfd.readouterr()
    assert out == test_output_labels4

test_output_labels5 = """\
Starting session: Foo Fighters (foo), song: Fighting Foo! (foo_01_song)
Calling hook 'oscHook1' sending OSC message '/session foo Foo Fighters' to port '8888'
Calling hook 'oscHook2' sending OSC message '/song foo_01_song Fighting Foo!' to port '8888'
Calling hook 'execHook1' running command 'echo foo "Foo Fighters"'
foo Foo Fighters
Calling hook 'execHook2' running command 'echo foo_01_song "Fighting Foo!"'
foo_01_song Fighting Foo!
Calling hook 'oscSongChange' sending OSC message '/song foo_01_song Fighting Foo!' to port '8888'
Calling hook 'oscSongChange' sending OSC message '/song foo_02_song Foo Lost' to port '8888'
Stopping session: Foo Fighters (foo), song: Foo Lost (foo_02_song)\n"""
@pytest.mark.parametrize("open_configuration", ["labels"], indirect = True)
def test_song_change_with_number_command_should_call_song_change_hook(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)

    studio_sh.do_start("foo")
    studio_sh.default("2")
    studio_sh.do_stop("")
    out, err = capfd.readouterr()
    assert out == test_output_labels5

test_error_hooks2 = """\
Error: MIDI port does not exist: test_alsaa.
Error: Incorrect MIDI channel value: 'a'.
Error: Incorrect MIDI channel value: 'a'.
Error: Incorrect MIDI message format: '['xx', '10', '127']'.
Error: Incorrect MIDI message format: '['cc', '100']'.
Error: Incorrect MIDI message format: '['pc', '100', '100']'.
Error: Incorrect hook definition: 'midiWrong'.\n"""
test_output_hooks2 = """\
Starting session: baz, song: baz_01_song01
Calling hook 'oscDummy1' sending OSC message '/load_session ./sessions/baz/baz_aux.slsess localhost:8123 /load_session' to port '8100'
Calling hook 'oscDummy2' sending OSC message '/load_midi_bindings ./sessions/baz/baz_aux.slb localhost:8123 /load_midi_bindings' to port '8100'
Calling hook 'midiAlsa' sending MIDI message 'pc 2' to port 'Midi Through' and channel '0'
Calling hook 'midiAlsa' sending MIDI message 'pc 2' to port 'test_alsa' and channel '0'
Calling hook 'midiJack' sending MIDI message 'pc 12' to port 'test_jack:in' and channel '0'
Calling hook 'execTest' running command 'echo execTest'
execTest
Calling hook 'oscDummy3' sending OSC message '/sl/-1/hit undo_all' to port '8100'
Calling hook 'oscDummy4' sending OSC message '/sl/-1/hit start' to port '8100'
Calling hook 'oscDummy5' sending OSC message '/EDIT tracks {visible:true}' to port '8100'
Calling hook 'midiAlsa' sending MIDI message 'cc 10 127' to port 'test_alsa' and channel '0'
Calling hook 'midiJack' sending MIDI message 'cc 100 0' to port 'test_jack:in' and channel '0'
Calling hook 'oscDummy6' sending OSC message '/set selected_loop_num 3' to port '8100'
Calling hook 'oscDummy7' sending OSC message '/set selected_loop_num 500' to port '8100'
Calling hook 'execTest' running command 'echo baz'
baz\n"""
@pytest.mark.parametrize("open_configuration", ["hooks"], indirect = True)
def test_running_hooks_with_valid_hooks_in_configuration_should_send_them(capfd, setupConfiguration):
    # create jack midi input port
    jack_in = rtmidi.MidiIn(name = "test_jack", rtapi = rtmidi.API_LINUX_ALSA)
    jack_in.open_virtual_port(name = "in")
    
    # create alsa midi input port
    alsa_in = rtmidi.MidiIn(name = "test_alsa", rtapi = rtmidi.API_UNIX_JACK)
    alsa_in.open_virtual_port(name = "in") 

    # create osc listener
    proc = Popen("oscdump -L 8100", shell = True, stdout = PIPE, stderr = PIPE)

    # start studiosh
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    studio_sh.do_start("baz")
    out, err = capfd.readouterr()

    # check jack midi message
    jack_message = jack_in.get_message()
    assert [0xC0, 12] in jack_message

    jack_message = jack_in.get_message()
    assert [0xB0, 100, 0] in jack_message
    jack_in.close_port()

    # check alsa midi message
    alsa_message = alsa_in.get_message()
    assert [0xC0, 2] in alsa_message

    alsa_message = alsa_in.get_message()
    assert [0xB0, 10, 127] in alsa_message
    alsa_in.close_port()

    # check osc message
    out_osc = None
    err_osc = None
    try:
        out_osc, err_osc = proc.communicate(timeout=2)
    except:
        proc.terminate()
        out_osc, err_osc = proc.communicate()

    # StudioSh out & err
    assert err == test_error_hooks2
    assert out == test_output_hooks2

    # osc out & err
    assert not err_osc.decode()
    out_osc_lines = out_osc.decode().split("\n")
    assert "/load_session sss \"./sessions/baz/baz_aux.slsess\" \"localhost:8123\" \"/load_session\"" in out_osc_lines[0]
    assert "/load_midi_bindings sss \"./sessions/baz/baz_aux.slb\" \"localhost:8123\" \"/load_midi_bindings\"" in out_osc_lines[1]
    assert "/sl/-1/hit s \"undo_all\"" in out_osc_lines[2]
    assert "/sl/-1/hit s \"start\"" in out_osc_lines[3]
    assert "/EDIT ss \"tracks\" \"{visible:true}\"" in out_osc_lines[4]
    assert "/set si \"selected_loop_num\" 3" in out_osc_lines[5]
    assert "/set si \"selected_loop_num\" 500" in out_osc_lines[6]

test_output3 = """\
Starting session: foo, song: foo_01_song
Starting program: sooperlooper -j sooperlooper_main -p 9951
Starting program: sooperlooper -j sooperlooper_aux -p 9952
Starting program: qjackctl
Starting program: jalv.gtk https://surge-synthesizer.github.io/lv2/surge-xt -n surge-xt
Starting program: open-stage-control -p 8123 -s localhost:9951 -t orange -c "./gui/studiosh.js" -l "./gui/studiosh.json"
Creating connection from \'surge-xt:lv2_audio_out_1\' to \'Comet Lake PCH-LP cAVS Speaker + Headphones:playback_FL\'.
Creating connection from \'surge-xt:lv2_audio_out_2\' to \'Comet Lake PCH-LP cAVS Speaker + Headphones:playback_FR\'.
Calling hook 'oscSLaux1' sending OSC message '/load_session ./sessions/foo/foo_aux.slsess localhost:8123 /load_session' to port '9952'
Calling hook 'oscSLaux2' sending OSC message '/load_midi_bindings ./sessions/foo/foo_aux.slb localhost:8123 /load_midi_bindings' to port '9952'
Creating connection from \'sooperlooper_aux:loop0_out_1\' to \'Comet Lake PCH-LP cAVS Speaker + Headphones:playback_FL\'.
Creating connection from \'sooperlooper_aux:loop1_out_1\' to \'Comet Lake PCH-LP cAVS Speaker + Headphones:playback_FL\'.
Creating connection from \'sooperlooper_aux:loop2_out_1\' to \'Comet Lake PCH-LP cAVS Speaker + Headphones:playback_FL\'.
Calling hook 'oscGUI' sending OSC message '/session foo' to port '8123'
Creating connection from \'sooperlooper_aux:loop3_out_1\' to \'Comet Lake PCH-LP cAVS Speaker + Headphones:playback_FL\'.
Calling hook 'oscGUI3' sending OSC message '/song foo_01_song' to port '8123'
Creating connection from \'sooperlooper_main:loop1_out_1\' to \'Comet Lake PCH-LP cAVS Speaker + Headphones:playback_FL\'.
Creating connection from \'sooperlooper_main:loop2_out_1\' to \'Comet Lake PCH-LP cAVS Speaker + Headphones:playback_FL\'.
Creating connection from \'sooperlooper_main:loop3_out_1\' to \'Comet Lake PCH-LP cAVS Speaker + Headphones:playback_FL\'.
Calling hook 'oscSLmain' sending OSC message '/set selected_loop_num 2' to port '9951'
Calling hook 'oscSLaux' sending OSC message '/set selected_loop_num 0' to port '9952'
Calling hook 'execTest' running command 'if pgrep qtile >/dev/null 2>&1; then qtile cmd-obj -o widget textbox -f update -a "<b>changing to session: foo</b>"; fi'\n"""
@pytest.mark.parametrize("open_configuration", ["full"], indirect = True)
def test_with_full_configuration_should_print_correct_output_and_no_errors(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)

    shutil.rmtree(TMP_DIR + "/sessions/foo", ignore_errors = True)
    os.makedirs(TMP_DIR + "/sessions/foo")
    shutil.copytree(APP_DIR + "/tests/assets/sessions/foo", TMP_DIR + "/sessions/foo", dirs_exist_ok = True)
    
    shutil.rmtree(TMP_DIR + "/gui", ignore_errors = True)
    os.makedirs(TMP_DIR + "/gui")
    shutil.copytree(APP_DIR + "/gui", TMP_DIR + "/gui", dirs_exist_ok = True)
    
    studio_sh.do_start("")
    out, err = capfd.readouterr()
    studio_sh.do_stop("")    

    shutil.rmtree(TMP_DIR + "/sessions/foo")
    shutil.rmtree(TMP_DIR + "/gui")

    assert out == test_output3
    assert not err

# recorder
@pytest.mark.parametrize("open_configuration", ["programs"], indirect = True)
def test_recorder_without_start_should_print_error(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    
    studio_sh.do_recorder("")
    out, err = capfd.readouterr()
    studio_sh.do_stop("")

    assert studio_sh is not None
    assert err == "Error: No session running. Run start first.\n" 

@pytest.mark.parametrize("open_configuration", ["programs"], indirect = True)
def test_recorder_with_empty_config_should_print_no_recorder_section_found_error(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    
    studio_sh.do_start("foo")
    studio_sh.do_recorder("")
    out, err = capfd.readouterr()
    studio_sh.do_stop("")

    assert studio_sh is not None
    assert err == "Error: No recorder section found in configuration file.\n"

@pytest.mark.parametrize("open_configuration", ["recorder"], indirect = True)
def test_recorder_with_no_recorder_section_for_current_session_in_config_should_print_no_recorder_section_found_for_session_error(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    
    studio_sh.do_start("baz")
    studio_sh.do_recorder("")
    out, err = capfd.readouterr()
    studio_sh.do_stop("")

    assert studio_sh is not None
    assert err == "Error: No recorder section for session 'baz' found in configuration file.\n"

test_output4 = """\
Starting session: foo, song: foo_01_song
Starting program: sooperlooper
Starting program: ardour8\n"""
@pytest.mark.parametrize("open_configuration", ["recorder"], indirect = True)
def test_recorder_with_recorder_section_in_config_should_start_ardour(capfd, setupConfiguration):
    studio_sh = StudioSh(root_dir = TMP_DIR, configuration = setupConfiguration)
    
    studio_sh.do_start("foo")
    studio_sh.do_recorder("")
    out, err = capfd.readouterr()
    studio_sh.do_stop("")

    assert studio_sh is not None
    assert not err
    assert out == test_output4

