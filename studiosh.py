import os
import sys
import re
import shlex
import time
import jack
import rtmidi
import os.path
import readline
from cmd import Cmd
from subprocess import Popen, DEVNULL
from pythonosc import udp_client

BANNER = "Hello! This is \033[3m\033[1mstudiosh\033[0m\033[0m. If you need help, type 'help'.\n----------------------------------------------------------"
DEFAULT_ROOT_DIR = "."
DEFAULT_SONG_ID = 1
LOG_OUT = "./logs/studiosh.log"
LOG_ERR = "./logs/studiosh_error.log"
HISTFILE = os.path.expanduser('./.studiosh_history')
HISTFILE_SIZE = 1000

class StudioSh(Cmd):
    'StudioSh class'

    # internal
    def __init__(self, root_dir = DEFAULT_ROOT_DIR, configuration = []):
            super().__init__()

            self.configuration = configuration
            self.started = False
            self.root_dir = root_dir
            self.session_name = "no session"
            self.session_label = ""
            self.song_name = "no song"
            self.song_label = ""
            self.intro = BANNER            
            self.prompt = "> "
            self.all_sessions = None
            self.all_songs = None
            self.started_programs = []
            self.log_out = sys.stdout
            self.log_err = sys.stderr
            self.debug = False
            self.jack_connect = None
            
            self._parseConfiguration()

    # helpers
    def _logInfo(self, message):
        if self.log_out:
            print(f"{message}", file = self.log_out, flush = True)
        else:
            print(f"Error: Could not write to {LOG_OUT}.", file = sys.stderr)

    def _logDebug(self, message):
        if self.debug:
            if self.log_out:
                print(f"Debug: {message}", file = self.log_out, flush = True)
            else:
                print(f"Error: Could not write to {LOG_OUT}.", file = sys.stderr)

    def _logError(self, message):
        if self.log_err:
            print(f"Error: {message}", file = self.log_err, flush = True)
        else:
            print(f"Error: Could not write to {LOG_ERR}.", file = sys.stderr)


    def _adios(self):
        self.do_stop("")
        print("Adios!")

    def _getPrevSession(self):
        return self.all_sessions[
            (self.all_sessions.index(self.session_name) - 1)
            % len(self.all_sessions)]

    def _getNextSession(self):
        return self.all_sessions[
            (self.all_sessions.index(self.session_name) + 1) 
            % len(self.all_sessions)]

    def _getPrevSong(self):
        return self.all_songs[
            (self.all_songs.index(self.song_name) - 1)
            % len(self.all_songs)]

    def _getNextSong(self):
        return self.all_songs[
            (self.all_songs.index(self.song_name) + 1) 
            % len(self.all_songs)]

    def _getAllSongs(self, session_name = None):
        if self.configuration:
            session_name = session_name or self.session_name
            
            if session_name in self.configuration["sessions"]:
                if self.configuration["sessions"][session_name]:
                    if "songs" in self.configuration["sessions"][session_name]:
                        all_songs = [list(song.keys())[0] if isinstance(song, dict) else song for song in self.configuration["sessions"][session_name]["songs"]]

                        if all_songs:
                            if len(all_songs) > 0:
                                self.all_songs = all_songs
                                return True

        return False

    def _parseSection(self, parent, section):
        if section in parent:
            steps = parent[section]
            if steps:
                for step in steps:
                    idx = steps.index(step)
                    step = list(step.keys())[0]
                    args = parent[section][idx][step]
                    if step == "startPrograms":
                        self._startPrograms(args)
                    elif step == "connectNodes":
                        self._connectNodes(args)
                    elif step == "callHooks":
                        self._callHooks(args)
                    elif step == "sleep":
                        if isinstance(args, int):
                            time.sleep(int(args))
                        else:
                            self._logError(f"Incorrect value for sleep step: {args}.")

    def _parseConfiguration(self):
        if self.configuration:
            if "sessions" in self.configuration:
                if isinstance(self.configuration["sessions"], dict):
                    self.all_sessions = list(self.configuration["sessions"].keys())
                    return
                    # TODO: reimplement
                    for session_section in self.all_sessions:
                        if self.configuration[session_section]:
                            all_songs = self.configuration[session_section]["songs"].keys()
                            for song_section in all_songs:
                                if self.configuration[session_section][song_section]:
                                    all_settings = self.configuration[session_section][song_section].keys()
                                    for setting_section in all_settings:
                                        if setting_section not in ["ProgramsToStart", "ConnectionsToMake"]:
                                            pass
                                            # self._logError("Invalid...")
                                else:
                                    pass
                                    # self._logError(f"Song section '{song_section}' empty.")
                        else:
                            pass
                            # self._logError(f"Session section '{session_section}' empty.")
        else:
            pass
            # self._logError(f"...")

    def _startPrograms(self, programs):
        if programs:
            for program in programs:
                if "${session_name}" in program:
                    program = program.replace("${session_name}", self.session_name)
                if "${session_id}" in program:
                    program = program.replace("${session_id}", str(self.all_sessions.index(self.session_name)))
                if "${song_name}" in program:
                    program = program.replace("${song_name}", self.song_name)
                if "${song_id}" in program:
                    program = program.replace("${song_id}", str(self.all_songs.index(self.song_name)))

                env = None

                if isinstance(program, dict):
                    program,envs_add = list(program.items())[0]
                    env = os.environ.copy()
                    for env_add in envs_add.split(" "):
                        env[env_add.split("=")[0]] = env_add.split("=")[1]

                self._logInfo(f"Starting program: {program}")
                try:
                    if self.debug == True:
                        out_file = self.log_out
                        err_file = self.log_err
                    else:
                        out_file = DEVNULL
                        err_file = DEVNULL

                    program_split = shlex.split(program)
                    proc = Popen(program_split, env = env, stdout = out_file, stderr = err_file)
                    time.sleep(1)
                    error = proc.poll()
                    if error:
                        self._logError(f"'{program_split[0]}': Returned error code: {error}")
                        continue
                    
                    self.started_programs.append(proc)
                    
                except OSError as e:
                    self._logError(f"'{program_split[0]}': {e.strerror}")
                except:
                    self._logError(f"{sys.exc_info()}.")
            
        try:
            self.jack_connect = jack.Client("studiosh")
            self._logDebug("Connecting studiosh to jack server.")
        except jack.JackOpenError as err:
            self.jack_connect = None
            self._logDebug(f"No jackd server running: {err}")
            
    def _connectNodes(self, connections):
        if connections:
            for connection in connections:
                if isinstance(connection, dict):
                    src_ptrn,dst_ptrn = list(connection.items())[0]

                    src = self.jack_connect.get_ports(name_pattern=src_ptrn)
                    if len(src) > 0:
                        src = src[0].name
                    else:
                        src = src_ptrn

                    dst = self.jack_connect.get_ports(name_pattern=dst_ptrn)
                    if len(dst) > 0:
                        dst = dst[0].name
                    else:
                        dst = dst_ptrn

                else:
                    self._logError(f"Connection definition must contain both src and dst strings.")
                    continue

                self._logInfo(f"Creating connection from '{src}' to '{dst}'.")
                try:
                    self.jack_connect.connect(src, dst)
                except jack.JackErrorCode:
                    self._logError(f"Failed to connect '{src}' to '{dst}'.")

    def _callHooks(self, hooks):
        if hooks:
            for hook in hooks:
                if isinstance(hook, dict):
                    name,args = list(hook.items())[0]
                    args = list(args) # IMPORTANT: copy value, don't just reference - otherwise you change value in configuration, not local one
                    
                    self._logDebug(f"Calling hook '{name}': {args}.")

                    if "osc" in name:
                        port = args[0]
                        self._logDebug(f"port: {port}")

                        message = args[1]
                        self._logDebug(f"message: {message}")

                        try:
                            message_split = message.split(" ")
                            message_updated = []
                            message_converted = []

                            # if the osc message contains quoted string put it to the split message list as single element
                            if any(re.search(r'^"', part) for part in message_split) and any(re.search(r'"$', part) for part in message_split):
                                self._logDebug(f"Has string")

                                inside_quoted_string = False
                                quoted_string = ''

                                for part in message_split:
                                    if re.search(r'^"', part):
                                        self._logDebug(f"Beginning: {part}")
                                        inside_quoted_string = True
                                        if quoted_string == '':
                                            quoted_string = part[1:]
                                        else:
                                            quoted_string = quoted_string + " " + part
                                    elif re.search(r'"$', part):
                                        self._logDebug(f"End: {part}")
                                        quoted_string = quoted_string + " " + part[:-1]
                                        message_updated.append(quoted_string)
                                        inside_quoted_string = False
                                    elif inside_quoted_string:
                                        self._logDebug(f"Inside: {part}")
                                        quoted_string = quoted_string + " " + part
                                    else:
                                        self._logDebug(f"Outside: {part}")
                                        message_updated.append(part)

                                self._logDebug(f"quoted_string: {quoted_string}")
                                self._logDebug(f"message_updated: {message_updated}")
                                self._logDebug(f"message_split: {message_split}")
                                message_split = message_updated
                                self._logDebug(f"message_split: {message_split}")

                            for part in message_split:
                                if part.lstrip("+-").isnumeric():
                                    message_converted.append(eval(part))
                                else:
                                    if "${session_name}" in part:
                                        message_converted.append(part.replace("${session_name}", self.session_name))
                                    elif "${session_label}" in part:
                                        message_converted.append(part.replace("${session_label}", self.session_label))
                                    elif "${session_id}" in part:
                                        message_converted.append(int(part.replace("${session_id}", str(self.all_sessions.index(self.session_name)))))
                                    elif "${song_name}" in part:
                                        message_converted.append(part.replace("${song_name}", self.song_name))
                                    elif "${song_label}" in part:
                                        message_converted.append(part.replace("${song_label}", self.song_label))
                                    elif "${song_id}" in part:
                                        message_converted.append(int(part.replace("${song_id}", str(self.all_songs.index(self.song_name)))))
                                    else:
                                        message_converted.append(str(part))

                            self._logInfo(f"Calling hook '{name}' sending OSC message '{' '.join([str(part) for part in message_converted if part != ''])}' to port '{port}'")
                            osc_client = udp_client.SimpleUDPClient("localhost", args[0])
                            osc_client.send_message(message_converted[0], message_converted[1:])
                        except Exception as e:
                            self._logError(f"e: {e}")
                            self._logError(f"Failed to call hook '{name}': '{args}'.")
                    elif "midi" in name:
                        port = args[0]
                        self._logDebug(f"port: {port}")

                        if isinstance(args[1], int):
                            channel = args[1]
                        else:
                            self._logError(f"Incorrect MIDI channel value: '{args[1]}'.")
                            continue
                        self._logDebug(f"channel: {channel}")

                        message = args[2]
                        self._logDebug(f"message: {message}")

                        message_split = message.split(" ")
                        message_converted = message_split
                        msg_type = message_split[0]
                        if msg_type == "pc" and len(message_split) == 2:
                            value = int(message_split[1])
                            message_converted = [0xC0 + channel, value]
                        elif msg_type == "cc" and len(message_split) == 3:
                            cc_num = int(message_split[1])
                            value = int(message_split[2])
                            message_converted = [0xB0 + channel, cc_num, value]
                        else:
                            self._logError(f"Incorrect MIDI message format: '{message_converted}'.")
                            continue

                        dst_port_idx = None

                        # first try alsa midi
                        try:
                            midi_out = rtmidi.MidiOut(rtapi=rtmidi.API_LINUX_ALSA, name = "studiosh_test")
                        except Exception as err:
                            self._logError(err)

                        ports = midi_out.get_ports()
                        for p in ports:
                            if re.search(port, p):
                                dst_port_idx = midi_out.get_ports().index(p)

                        # if port not in alsa ports
                        # then try jack midi
                        if dst_port_idx == None:
                            try:
                                midi_out = rtmidi.MidiOut(rtapi=rtmidi.API_UNIX_JACK, name = "studiosh_test")
                            except Exception as err:
                                self._logError(err)

                            ports = midi_out.get_ports()
                            for p in ports:
                                if re.search(port, p):
                                    dst_port_idx = midi_out.get_ports().index(p)

                        # if port found, send message
                        if dst_port_idx != None:
                            try:
                                self._logInfo(f"Calling hook '{name}' sending MIDI message '{message}' to port '{port}' and channel '{channel}'")
                                midi_out.open_port(dst_port_idx, name = "out")
                                midi_out.send_message(message_converted)
                            except Exception as err:
                                self._logError(err)

                            time.sleep(1)

                            midi_out.close_port()
                        else:
                            self._logError(f"MIDI port does not exist: {port}.")

                    elif "exec" in name:
                        command = args[0]
                        
                        try:
                            if "${session_name}" in command:
                                command = command.replace("${session_name}", self.session_name)
                            if "${session_label}" in command:
                                command = command.replace("${session_label}", "\"" + self.session_label + "\"")
                            if "${session_id}" in command:
                                command = command.replace("${session_id}", str(self.all_sessions.index(self.session_name)))
                            if "${song_name}" in command:
                                command = command.replace("${song_name}", self.song_name)
                            if "${song_label}" in command:
                                command = command.replace("${song_label}", "\"" + self.song_label + "\"")
                            if "${song_id}" in command:
                                command = command.replace("${song_id}", str(self.all_songs.index(self.song_name)))
 
                            self._logDebug(f"command: {command}")
                            self._logInfo(f"Calling hook '{name}' running command '{command}'")
                            Popen(command, shell = True)
                            time.sleep(1)
                            
                        except:
                            self._logError(f"Failed to call hook '{command}'.")

                else:
                    self._logError(f"Incorrect hook definition: '{hook}'.")

    def _stopPrograms(self):
        for proc in reversed(self.started_programs):
            self._logInfo("Stopping program: " + " ".join(proc.args))
            proc.terminate() # seems better than kill, terminates also procs's subprocesses
            proc.communicate()  # IMPORTANT: collecting proc exit status to get rid of zombie/defunct after kill
            self.started_programs.remove(proc)

        os.system('if pgrep qtile >/dev/null 2>&1; then qtile cmd-obj -o widget textbox -f update -a "<b>no session / no song</b>"; fi')

    def _changeSession(self):
        if "label" in self.configuration["sessions"][self.session_name]:
            self.session_label = self.configuration["sessions"][self.session_name]["label"]

        if "changeSession" in self.configuration:
            sections = self.configuration["changeSession"]
            if sections:
                for section in sections:
                    if isinstance(section, dict):
                        section_name = list(section.keys())[0]
                        
                        if self.session_name in section_name:
                            self._parseSection(section, section_name)
            
        self._changeSong()

    def _changeSong(self):
        song_id = self.all_songs.index(self.song_name)
        if isinstance(self.configuration["sessions"][self.session_name]["songs"][song_id], dict):
            self.song_label = list(self.configuration["sessions"][self.session_name]["songs"][song_id].values())[0]

        if "changeSong" in self.configuration:
            sections = self.configuration["changeSong"]
            if sections:
                for section in sections:
                    if isinstance(section, dict):
                        section_name = list(section.keys())[0]

                        section_name_split = section_name.split(";")
                        if self.song_name in section_name or self.session_name in section_name_split:
                            self._parseSection(section, section_name)

    # commands
    def do_start(self, args):    
        'Starts a session: start [session_name] [song_id]\nIf session_name not provided, will start first session defined in configuration file.\nIf song_id not provided, will start 1st song.\nIt is also possible to start a session by typing it\'s name.\nWill parse "start" section in configuration file looking for programs to start, connections to make and hooks to call.'

        if self.started:
            self._logDebug("Session already started.")
            return False

        os.system('if pgrep qtile >/dev/null 2>&1; then qtile cmd-obj -o widget textbox -f update -a "<b>no session / no song</b>"; fi')
        sess_dir = ""
        session_name = ""
        song_id = 0
        all_songs = []
        
        if not args or args == "":
            args = []
        else:
            args = args.split(" ")
    
        if len(args) == 2:
            session_name = args[0]
            song_id = int(args[1])
        elif len(args) == 1:
            if args[0].lstrip("+-").isnumeric():
                if self.all_sessions:
                    session_name = self.all_sessions[0]
                song_id = int(args[0])
            else:
                session_name = args[0]
                song_id = DEFAULT_SONG_ID
        elif len(args) == 0:
            if self.all_sessions:
                session_name = self.all_sessions[0]
            song_id = DEFAULT_SONG_ID
        else:
            self._logError("Too many arguments.")
            return False

        if not self.all_sessions:
            self._logError("No session found in configuration file.")
            return False

        if not self._getAllSongs(session_name):
            self._logError(f"Session '{session_name}' has no songs.")
            return False
            
        # set session & song name
        if song_id < 1 or song_id > len(self.all_songs):
            self._logError(f"Incorrect song number for session '{session_name}': {song_id}.")
            return False
        else:
            self.started = True            

            self.session_name = session_name

            if "label" in self.configuration["sessions"][self.session_name]:
                self.session_label = self.configuration["sessions"][self.session_name]["label"]

            self.song_name = self.all_songs[song_id - 1]

            if isinstance(self.configuration["sessions"][self.session_name]["songs"][song_id - 1], dict):
                self.song_label = list(self.configuration["sessions"][self.session_name]["songs"][song_id - 1].values())[0]

            if "start" in self.configuration:
                sections = self.configuration["start"]
                if self.session_label and self.song_label:
                    self._logInfo(f"Starting session: {self.session_label} ({self.session_name}), song: {self.song_label} ({self.song_name})")
                elif self.session_label and not self.song_label:
                    self._logInfo(f"Starting session: {self.session_label} ({self.session_name}), song: {self.song_name}")
                elif not self.session_label and self.song_label:
                    self._logInfo(f"Starting session: {self.session_name}, song: {self.song_label} ({self.song_name})")
                elif not self.session_label and not self.song_label:
                    self._logInfo(f"Starting session: {self.session_name}, song: {self.song_name}")

                if sections:
                    for section in sections:
                        if isinstance(section, dict):
                            section_name = list(section.keys())[0]
                            
                            if self.session_name in section_name:
                                self._parseSection(section, section_name)
            
                self._changeSession()
        
    def do_sta(self, args):
        'Shortcut for start (see help start).'
        self.do_start(args)

    def do_stop(self, args):
        'Stops currently running session: stop.'
        if not self.started:
            self._logDebug("No session started yet.")
            return False

        if self.session_label and self.song_label:
            self._logInfo(f"Stopping session: {self.session_label} ({self.session_name}), song: {self.song_label} ({self.song_name})")
        elif self.session_label and not self.song_label:
            self._logInfo(f"Stopping session: {self.session_label} ({self.session_name}), song: {self.song_name}")
        elif not self.session_label and self.song_label:
            self._logInfo(f"Stopping session: {self.session_name}, song: {self.song_label} ({self.song_name})")
        elif not self.session_label and not self.song_label:
            self._logInfo(f"Stopping session: {self.session_name}, song: {self.song_name}")

        self._stopPrograms()
        self.started = False
        self.session_name = "no session"
        self.all_songs = []
        self.song_name = "no song"

    def do_sto(self, args):
        'Shortcut for stop (see help stop).'
        self.do_stop(args)

    def do_restart(self, args):
        'Restarts session: restart [session_name] [song_id].\nIf session_name provided, restarts to session_name, if not, restarts to current session.\nIf song_id provided, restarts to song_id, if not, restarts to 1st song.'
        curr_session = None
        curr_song_id = None
        if self.started:
            curr_session = self.session_name
            curr_song_id = self.all_songs.index(self.song_name) + 1
            curr_session_and_song_id = curr_session + " " + str(curr_song_id)

        self.do_stop("")
        self.do_start(args or curr_session_and_song_id)

    def do_res(self, args):
        'Shortcut for restart (see help restart).'
        self.do_restart(args)

    def do_reboot(self, args):
        'Reboots system, stopping session programs first: reboot.'
        self.do_stop("")
        os.system("nohup reboot")

    def do_reb(self, args):
        'Shortcut for reboot (see help reboot).'
        self.do_reboot(args)

    def do_poweroff(self, args):
        'Powers system off, stopping session programs first: poweroff.'
        self.do_stop("")
        os.system("nohup poweroff")

    def do_off(self, args):
        'Shortcut for poweroff (see help poweroff).'
        self.do_poweroff(args)

    def do_session(self, args):
        'Prints name of current session: session.'
        if not self.started:
            self._logError("No session running. Run start first.")
            return False

        self._logInfo(f"session: {self.session_name}.")

    def do_sessions(self, args):
        'Prints names of all sessions defined in configuration file: sessions.'
        self._logInfo(f"sessions ({len(self.all_sessions)}): {', '.join(self.all_sessions)}.")

    def do_song(self, args):
        'Prints name of current song: song.'
        if not self.started:
            self._logError("No session running. Run start first.")
            return False
            
        self._logInfo(f"song: {self.song_name}.")

    def do_songs(self, args):
        'Prints names of all songs of current session: songs.'
        if not self.started:
            self._logError("No session running. Run start first.")
            return False

        self._logInfo(f"songs ({len(self.all_songs)}): {', '.join(self.all_songs)}.")

    def do_connections(self, args):
        'Prints all connections of programs started by current session as defined in configuration file: connections.'

        if not self.started:
            self._logError("No session running. Run start first.")
            return False
            
        if args == "":
            self._logError("Missing argumment: list of program names.")
            return False

        args = args.split(" ")
        self._logInfo("ConnectionsToMake: {")
        for program in args:
            self._logInfo(f"    # {program}")
            program_inports = self.jack_connect.get_ports(name_pattern = program, is_input = True)
            program_outports = self.jack_connect.get_ports(name_pattern = program, is_output = True)
            for port in program_inports:
                connected_port = self.jack_connect.get_all_connections(port)
                if len(connected_port) > 0:
                    self._logInfo(f"    '{connected_port[0].name}': '{port.name}',")
            for port in program_outports:
                connected_port = self.jack_connect.get_all_connections(port)
                if len(connected_port) > 0:
                    self._logInfo(f"    '{port.name}': '{connected_port[0].name}',")
        self._logInfo("    }")

    def do_nexts(self, args):
        'Changes session to next one, as defined in configuration file: nexts.'
        if not self.started:
            self._logError("No session running. Run start first.")
            return False

        self.session_name = self._getNextSession()
        if not self._getAllSongs():
            self._logError(f"Session '{self.session_name}' has no songs.")
            return False        

        self.song_name = self.all_songs[0]

        self._changeSession()

    def do_ns(self, args):
        'Shortcut for nexts (see help nexts).'
        self.do_nexts(args)

    def do_prevs(self, args):
        'Changes to session to previous one, as defined in configuration file: prevs.'
        if not self.started:
            self._logError("No session running. Run start first.")
            return False

        self.session_name = self._getPrevSession()
        if not self._getAllSongs():
            self._logError(f"Session '{session_name}' has no songs.")
            return False      

        self.song_name = self.all_songs[0]

        self._changeSession()

    def do_ps(self, args):
        'Shortcut for prevs (see help prevs).'
        self.do_prevs(args)

    def do_next(self, args):
        'Changes song to next one, as defined in configuration file: next.'
        if not self.started:
            self._logError("No session running. Run start first.")
            return False
        
        self.song_name = self._getNextSong()

        self._changeSong()

    def do_n(self, args):
        'Schortcut for next (see help next).'
        self.do_next(args)

    def do_prev(self, args):
        'Changes song to previous one, as defined in configuration file: prev.'
        if not self.started:
            self._logError("No session running. Run start first.")
            return False

        self.song_name = self._getPrevSong()

        self._changeSong()

    def do_p(self, args):
        'Shortcut for prev (see help prev).'
        self.do_prev(args)

    def do_recorder(self, args):
        'Starts recording program, calls hooks and makes connections as defined in recorder section in configuration file: recorder.'
        if not self.started:
            self._logError("No session running. Run start first.")
            return False

        if self.configuration:
            if "recorder" in self.configuration:
                sections = self.configuration["recorder"]
                
                if sections:
                    for section in sections:
                        if isinstance(section, dict):
                            section_name = list(section.keys())[0]
                            
                            if self.session_name in section_name:
                                self._parseSection(section, section_name)
                            else:
                                self._logError(f"No recorder section for session '{self.session_name}' found in configuration file.")
            else:
                self._logError("No recorder section found in configuration file.")

    def do_rec(self, args):
        'Shortcut for recorder (see help recorder).'
        self.do_recorder(args)

    def do_EOF(self, args):
        'Quits studiosh by pressing Ctrl + D.'
        self._adios()
        return True

    def do_quit(self, args):
        'Quits studiosh: quit.'
        self._adios()
        return True

    def do_q(self, args):
        'Shortcut for quit (see help quit).'
        self._adios()
        return True

    def do_exit(self, args):
        'Quits studiosh: exit.'
        self._adios()
        return True

    def do_e(self, args):
        'Shortcut for exit (see help exit).'
        self._adios()
        return True

    def do_bye(self, args):
        'Quits studiosh: bye.'
        self._adios()
        return True

    def do_adios(self, args):
        'Quits studiosh: adios.'
        self._adios()
        return True        

    def do_a(self, args):
        'Shortcut for adios (see help adios).'
        self._adios()
        return True

    def do_b(self, args):
        'Shortcut for bye (see help bye).'
        self._adios()
        return True

    def do_debug(self, args):
        'Turns debug prints on or off: debug 1|True|true|on|0|False|false|off'
        if args:
            if args in ["1", "True", "true", "on"]:
                self.debug = True
            elif args in ["0", "False", "false", "off"]:
                self.debug = False
            else:
                self._logError("Incorrect value for debug.")

        self._logInfo(self.debug)
    
    def do_deb(self, args):
        'Shortucut for debug (see help debug).'
        self.do_debug(args)
            
    def default(self, args):
        # if session name & optionally song id
        if any(session in args for session in self.all_sessions):
            if self.started:
                # if sesssion name & song id
                if " " in args:
                    self.session_name = args.split(" ")[0]
                    if not self._getAllSongs():
                        self._logError(f"Session '{self.session_name}' has no songs.")
                        return False
                    self.song_name = self.all_songs[int(args.split(" ")[1]) - 1]
                # if only session name
                else:
                    self.session_name = args
                    if not self._getAllSongs():
                        self._logError(f"Session '{self.session_name}' has no songs.")
                        return False        
                    self.song_name = self.all_songs[0]
                
                self._changeSession()
            else:
                self.do_start(args)
        # if only song id
        elif args.lstrip("+-").isdigit():
            if not self.started:
                self.do_start(args)
            else:
                self.song_name = self.all_songs[int(args) - 1]

                self._changeSong()
        # if anything else
        else:
            self._logError(f"Unrecognized command: '{args}'.")
            return False

    def emptyline(self):
        return ""

    def preloop(self):
        if readline and os.path.exists(HISTFILE):
            readline.read_history_file(HISTFILE)

    def postloop(self):
        if readline:
            readline.set_history_length(HISTFILE_SIZE)
            readline.write_history_file(HISTFILE)

    @property
    def session_name(self):
        return self._session_name

    @session_name.setter
    def session_name(self, val):
        self._session_name = val

    @property
    def song_name(self):
        return self._song_name

    @song_name.setter
    def song_name(self, val):
        self._song_name = val

    @property
    def songs_cnt(self):
        if self.all_songs:
            return len(self.all_songs)
        else:
            return None
